import json
import tempfile

from django.test import TestCase, override_settings

from stageit_ui.scripts import sync_grafana
from stageit_ui.models import StatisticsDashboard
from unittest.mock import patch, mock_open


class DashBoardRetrieveTestCase(TestCase):
    dashboards_response = ([{"uid": "abcd", "title": "Files", "url": "/stageit-grafana/d/Tt4l-amVk/files"},
                            {"uid": "defg", "title": "Requests", "url": "/stageit-grafana/d/rn3KlNiVk/requests"},
                            {"uid": "ghij", "title": "Users", "url": "/stageit-grafana/d/x9s8lmkVz/users"}])

    @patch('stageit_ui.scripts.sync_grafana.get')
    def test_add_grafana_dashboards(self, mock_get):
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = self.dashboards_response

        dashboards = sync_grafana.retrieve_dashboards_info()
        sync_grafana.update_create_dashboards(dashboards)
        dashboards = StatisticsDashboard.objects.all()
        self.assertIs(len(dashboards), 3)

    @patch('stageit_ui.scripts.sync_grafana.get')
    def test_update_and_write_grafana_dashboards(self, mock_get):
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = self.dashboards_response

        # create initial database entry
        StatisticsDashboard.objects.create(uid="abcd", title="Initial", sources="test")

        # update dashboard with uid 'abcd', create dashboards with uids 'defg' and 'ghij'
        dashboards = sync_grafana.retrieve_dashboards_info()
        sync_grafana.update_create_dashboards(dashboards)
        dashboards = StatisticsDashboard.objects.all()
        dashboard_uids = [dashboard.uid for dashboard in dashboards]
        dashboard_titles = [dashboard.title for dashboard in dashboards]

        self.assertListEqual(dashboard_uids, ["abcd", "defg", "ghij"], "Dashboard uids don't match")
        self.assertListEqual(dashboard_titles, ['Files', 'Requests', 'Users'], "Dashboard titles don't match")

    @patch('stageit_ui.scripts.sync_grafana.get')
    def test_write_no_grafana_dashboards_when_empty(self, mock_get):
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = []

        dashboards = sync_grafana.retrieve_dashboards_info()
        sync_grafana.update_create_dashboards(dashboards)
        dashboards = StatisticsDashboard.objects.all()
        self.assertIs(len(dashboards), 0)

    @patch('stageit_ui.scripts.sync_grafana.get')
    def test_write_no_grafana_dashboards_when_error_response(self, mock_get):
        mock_get.return_value.ok = False
        mock_get.return_value.json.return_value = []

        dashboards = sync_grafana.retrieve_dashboards_info()
        sync_grafana.update_create_dashboards(dashboards)
        dashboards = StatisticsDashboard.objects.all()
        self.assertIs(len(dashboards), 0)


temp_file = tempfile.TemporaryDirectory("test")


@override_settings(LOCAL_GRAFANA_PROVISIONING_PATH=temp_file.name,
                   GRAFANA_URL_PATH="/grafana-is-here/",
                   HOST_URL="http://example.com")
class DashBoardProvisionTestCase(TestCase):
    dashboard_json = {"meta": {"to-ignore": "ignored"}, "dashboard": {"x-men": "wolverine"}}
    file_content_mock = """{"x-men": "wolverine"}"""
    second_file_mock_content = """{"x-men": "batman"}"""

    def setUp(self):
        StatisticsDashboard.objects.create(uid="abcd", title="Initial", sources="test")
        StatisticsDashboard.objects.create(uid="defg", title="Second", sources="http://example.com")

    def tearDown(self):
        try:
            temp_file.cleanup()
        except OSError:
            print("didn't delete test tmp dir")
            pass

    def test_get_uids(self):
        uids = sync_grafana.get_uids()
        self.assertListEqual(uids, ["abcd", "defg"])

    @patch('stageit_ui.scripts.sync_grafana.get')
    def test_get_grafana_dashboard_json(self, mock_get):
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = self.dashboard_json
        uid = "abcd"

        dashboard = sync_grafana.get_grafana_dashboard(uid)
        self.assertIsNotNone(dashboard)
        self.assertEqual(self.dashboard_json['dashboard'], dashboard)

    @patch("stageit_ui.scripts.sync_grafana.open", new=mock_open(read_data=file_content_mock))
    def test_get_local_dashboard(self):
        uid = "abcd"

        local_json = sync_grafana.read_local_dashboard(uid)
        self.assertIsNotNone(local_json)
        self.assertEqual(json.loads(self.file_content_mock), local_json)

    @patch('stageit_ui.scripts.sync_grafana.get')
    @patch("stageit_ui.scripts.sync_grafana.open", new=mock_open(read_data=second_file_mock_content))
    def test_updated_grafana_dashboard(self, mock_get):
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = self.dashboard_json

        updated, _ = sync_grafana.update_local_grafana_dashboards("abcd")
        self.assertTrue(updated)

    @patch('stageit_ui.scripts.sync_grafana.get')
    @patch("stageit_ui.scripts.sync_grafana.open", new=mock_open(read_data=file_content_mock))
    def test_not_updated_grafana_dashboard(self, mock_get):
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = self.dashboard_json

        updated, _ = sync_grafana.update_local_grafana_dashboards("abcd")
        self.assertFalse(updated)

    @patch('stageit_ui.scripts.sync_grafana.get')
    def test_new_grafana_dashboard(self, mock_get):
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = self.dashboard_json

        updated, _ = sync_grafana.update_local_grafana_dashboards("new-uid")
        self.assertTrue(updated)
