import json
import logging
import os
from urllib.parse import urljoin

import requests
from django.conf import settings

from stageit_ui.models import StatisticsDashboard

logger = logging.getLogger(__file__)


def get(url):
    logger.info("GET Request done with url: %s", url)
    response = requests.get(url)
    if response.ok:
        return response
    return None


def retrieve_dashboards_info():
    grafana_url = urljoin(settings.HOST_URL, settings.GRAFANA_URL_PATH)
    grafana_api_dashboard_info_path = "api/search?type=dash-db"
    response = get(urljoin(grafana_url, grafana_api_dashboard_info_path))
    if response is None:
        return None

    dashboards_data = response.json()
    if dashboards_data is None:
        return None

    return dashboards_data


def update_create_dashboards(dashboards):
    if dashboards is None or len(dashboards) == 0:
        logger.info("There are no dashboards present in grafana")
        StatisticsDashboard.objects.all().delete()
        return
    for dashboard in dashboards:
        uid = dashboard['uid']
        _, created = StatisticsDashboard.objects.update_or_create(uid=uid, defaults=dict(
            title=dashboard['title'],
            sources=dashboard['url']))
        if created:
            logger.info("Created dashboard entry with uid: %s", uid)
        else:
            logger.info("Updated dashboard entry with uid: %s", uid)


def get_uids():
    dashboards = StatisticsDashboard.objects.all()
    return [dashboard.uid for dashboard in dashboards]


def get_grafana_dashboard(uid):
    grafana_url = urljoin(settings.HOST_URL, settings.GRAFANA_URL_PATH)
    grafana_api_dashboard_uid_path = "api/dashboards/uid/" + uid
    grafana_dashboard_uid = urljoin(grafana_url, grafana_api_dashboard_uid_path)

    response = get(grafana_dashboard_uid)
    if response is None:
        return

    dashboard_json = response.json()
    if dashboard_json is None or dashboard_json["dashboard"] is None:
        return

    return dashboard_json["dashboard"]


def read_local_dashboard(uid):
    file_path = settings.LOCAL_GRAFANA_PROVISIONING_PATH + "dashboards/" + uid + ".json"
    logger.info("Reading local dashboard file with path '%s'", file_path)
    try:
        with open(file_path, 'r') as _file:
            data = json.loads(_file.read())
        return data
    except FileNotFoundError:
        logging.info("No local version of the dashboard found with uid: %s. Creating a new one", uid)
        return {}


def replace_dashboard(uid, dashboard):
    file_path = settings.LOCAL_GRAFANA_PROVISIONING_PATH + "dashboards/" + uid + ".json"
    logger.info("Replacing or creating local dashboard file with path '%s'", file_path)
    try:
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, 'w') as _file:
            json.dump(dashboard, _file)
        return True
    except PermissionError as error:
        logger.exception(error)
        logger.error("You are not allowed to write to '%s'.\nYou have to run this script again", file_path)
        return False


def update_local_grafana_dashboards(uids):
    replaced_uids = []
    for uid in uids:
        grafana_dashboard = get_grafana_dashboard(uid)
        local_dashboards = read_local_dashboard(uid)
        if not grafana_dashboard == local_dashboards:
            replace_success = replace_dashboard(uid, grafana_dashboard)
            if replace_success:
                replaced_uids.append(uid)

    return len(replaced_uids) != 0, replaced_uids


def run():
    """
    This program synchronizes the dashboards in Grafana with the database and puts them on the server for provisioning.
    The following steps are executed:
    1. retrieval of the basic dashboard information
    2. update or create the dashboard information w.r.t. the database
        this ensures that the scripts in the GUI will be up-to-date
    3. retrieval of all the dashboards' configuration based on the uids present in the database
    4. update or creation of the dashboards' configuration on the server as per Grafana's provisioning standard
    """
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

    dashboards_data = retrieve_dashboards_info()
    update_create_dashboards(dashboards_data)

    uids = get_uids()
    updated, replaced_uids = update_local_grafana_dashboards(uids)
    if updated:
        logging.info("Local scripts replaced or created by grafana's version for uids: {}"
                     .format(', '.join(map(str, replaced_uids))))
    else:
        logging.info("No local scripts updated or created")
