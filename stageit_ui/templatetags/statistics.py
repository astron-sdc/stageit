from django import template
from stage_api.utils import get_retried_request_ids_per_lta_site as get_retried_request_ids_per_lta_site_util

register = template.Library()


@register.simple_tag
def get_retried_request_ids_per_lta_site():
    return get_retried_request_ids_per_lta_site_util()
