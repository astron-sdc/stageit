from django import template

register = template.Library()

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key, '')


@register.filter
def sort(list_collection):
    return sorted(list_collection)


@register.filter
def sort_reverse(list_collection):
    return sorted(list_collection, reverse=True)