from django import template
import humanize
register = template.Library()


@register.filter
def format_filesize(value):
    return humanize.naturalsize(value)

