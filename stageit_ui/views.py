from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView, FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django_filters import filters
from django_filters.filterset import FilterSet
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet

from stage_api.models.draft import DraftLTARequest
from stage_api.models.lta import LTARequest, LTARequestStatus, LTARequestStatusCode, LTASite
from stageit_ui.forms import StageRequestForm, StatisticDashboardForm
from stageit_ui.models import StatisticsDashboard
from stageit_ui.serializers import StatisticsSerializer


class DashboardFilter(FilterSet):
    lta_sites = filters.ModelChoiceFilter(field_name="lta_sites", queryset=LTASite.objects.all())

    class Meta:
        model = LTARequest
        fields = ['user__username', 'request_type', 'current_status_code', 'user__id']


class InsertRequest(FormView):
    template_name = 'stageit_ui/insert.html'
    form_class = StageRequestForm

    def get_success_url(self):
        return reverse('dashboard')

    def form_valid(self, form):
        form.insert_request(self.request.user)
        return super().form_valid(form)

@require_GET
@login_required
def claim_draft_request(request, pk):
    """
    Claims a draft request, converting it to regular LTA request

    TODO: this seems similar to the functionality provided by:
    stage_api.views.DraftLTARequestViewset.claim
    """
    user = request.user
    try:
        draft_request: DraftLTARequest = DraftLTARequest.objects.get(pk=pk)
        lta_request = draft_request.create_request(user.pk)
        return redirect('request-details', lta_request.pk)
    except DraftLTARequest.DoesNotExist:
        return redirect('dashboard')

@require_GET
@login_required()
def restart_request(request, pk):
    user = request.user
    try:
        if user.is_staff or request.user == user or user.is_superuser:
            lta_request: LTARequest = LTARequest.objects.get(pk=pk)
            lta_request.retry_in_place()
            redirect_to_details = request.GET.get('to_details', 'false').lower()
            if redirect_to_details == 'true':
                return redirect('request-details', lta_request.pk)
            else:
                return redirect('dashboard')
        else:
            return redirect('dashboard')
    except LTARequest.DoesNotExist:
        return redirect('dashboard')

@require_GET
@login_required()
def abort_request(request, pk):
    user = request.user

    try:
        if user.is_staff or request.user == user:
            lta_request: LTARequest = LTARequest.objects.get(pk=pk)
            lta_request.abort(user)
            redirect_to_details = request.GET.get('to_details', 'false').lower()
            if redirect_to_details == 'true':
                return redirect('request-details', lta_request.pk)
            else:
                return redirect('dashboard')
        else:
            return redirect('dashboard')
    except LTARequest.DoesNotExist:
        return redirect('dashboard')


class Dashboard(ListView):
    template_name = 'stageit_ui/dashboard.html'
    model = LTARequest
    paginate_by = 20
    ordering = ['-when']

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return self.model.objects.none()
        elif self.request.user.is_staff:
            return DashboardFilter(self.request.GET, super().get_queryset()).qs
        else:
            return DashboardFilter(self.request.GET, super().get_queryset().filter(user=self.request.user)).qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = DashboardFilter(self.request.GET, queryset=self.get_queryset())
        return context


class UserProfile(TemplateView, LoginRequiredMixin):
    template_name = 'account/profile.html'


class RequestDetails(DetailView):
    template_name = 'stageit_ui/request_details.html'
    model = LTARequest


class Statistics(ListView):
    template_name = 'stageit_statistics/dashboard.html'
    model = StatisticsDashboard

    def get_queryset(self):
        if not self.request.user.is_staff:
            return self.model.objects.none()
        else:
            return StatisticsFilter(self.request.GET, super().get_queryset()).qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = StatisticsFilter(self.request.GET, queryset=self.get_queryset())
        return context


class StatisticDashboard(ModelViewSet):
    queryset = StatisticsDashboard.objects.all()
    serializer_class = StatisticsSerializer
    permission_classes = [IsAdminUser]


class StatisticsFilter(FilterSet):
    class Meta:
        model = StatisticsDashboard
        fields = ['title', 'subtitle', 'sources']


class InsertStatisticDashboard(FormView):
    template_name = 'stageit_statistics/insert.html'
    form_class = StatisticDashboardForm

    def get_success_url(self):
        return '/statistics/'

    def form_valid(self, form):
        form.insert_dashboard()
        return super().form_valid(form)
