# Generated by Django 4.1 on 2022-08-30 12:23

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StatisticsDashboard',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=15)),
                ('subtitle', models.CharField(max_length=100)),
                ('sources', models.TextField(null=True)),
            ],
        ),
    ]
