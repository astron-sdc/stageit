import json

from django import forms
from django.db import transaction

from stage_api.models.lta import LTARequestType, LTARequest, AffectedSurls
from stageit_ui.models import StatisticsDashboard


class StageRequestForm(forms.Form):
    surls = forms.CharField(widget=forms.Textarea, label='SURLs')
    email = forms.EmailField(required=False,
                             help_text='Set alternative email to receive notification if left empty your user'
                                       ' email will be used')
    notify = forms.BooleanField(help_text='Notify me with an email when the request changes status', required=False)

    def insert_request(self, user):
        # TODO: check models.link_surls_to_lta_request & DraftLTARequest.create_request
        # Seems similar to this method

        valid_surls = [surl.rstrip('\n\r') for surl in self.cleaned_data['surls'].splitlines(keepends=False) if
                       surl.startswith('srm://')]
        with transaction.atomic():
            lta_request = LTARequest(request_type=LTARequestType.STAGE, email=self.cleaned_data['email'],
                                     notify=self.cleaned_data['notify'], user=user)
            lta_request.save()

            affected_surls = [AffectedSurls(surl=surl, request=lta_request) for surl in valid_surls]
            for a_surl in affected_surls:
                a_surl.update_project_info()

            AffectedSurls.objects.bulk_create(affected_surls)
            lta_request.set_lta_sites_from_surls()


class StatisticDashboardForm(forms.Form):
    title = forms.CharField(widget=forms.TextInput, label='Title')
    subtitle = forms.CharField(widget=forms.TextInput, required=False, label="Subtitle")
    sources = forms.CharField(widget=forms.Textarea, label='Sources',
                              help_text='These sources are loaded into an iframe, split by \',\'')

    def insert_dashboard(self):
        with transaction.atomic():
            sources = self.cleaned_data['sources']
            formatted_sources = json.dumps(sources)
            dashboard = StatisticsDashboard(title=self.cleaned_data['title'], subtitle=self.cleaned_data['subtitle'],
                                            sources=formatted_sources)
            dashboard.save()
