from django.db import models


class StatisticsDashboard(models.Model):
    uid = models.CharField(primary_key=True, max_length=100, editable=False, unique=True, blank=True)
    title = models.CharField(max_length=15, default='')
    subtitle = models.CharField(max_length=100)
    sources = models.TextField(null=True)

    def get_sources_as_list(self):
        unquoted = self.sources.strip('"')
        return unquoted.split(",")
