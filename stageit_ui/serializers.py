from rest_framework.serializers import ModelSerializer

from stageit_ui.models import StatisticsDashboard


class StatisticsSerializer(ModelSerializer):
    class Meta:
        model = StatisticsDashboard
        fields = '__all__'
