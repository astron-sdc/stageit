from unittest.mock import patch

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from stage_api.models.lta import LTARequest, LTARequestStatus


class TestDraftRequests(TestCase):

    @patch("stage_api.models.lta.celery_app.send_task")
    def setUp(self, mock) -> None:
        self.admin = User(username='admin', is_superuser=True)
        self.admin.save()
        self.staff = User(username='staff', is_staff=True)
        self.staff.save()
        self.user = User(username='simple')
        self.user.save()
        self.creator = User(username='simple2')
        self.creator.save()

        self.request = LTARequest(request_type="stage", user=self.creator)
        self.request.save()
        ltstatus = LTARequestStatus(request=self.request, status_code="E")
        ltstatus.save()
        self.request.current_status_code = "E"
        self.request.save()

    def login_as_admin(self):
        self.client.force_login(self.admin)

    def login_as_staff(self):
        self.client.force_login(self.staff)

    def login_as_user(self):
        self.client.force_login(self.user)

    def login_as_creator(self):
        self.client.force_login(self.creator)

    def logout(self):
        self.client.logout()

    @patch("stage_api.models.lta.celery_app.send_task")
    def test_restart_not_logged_in(self, st_mock):
        self.logout()
        url = reverse("request-details-restart", args=[self.request.pk])
        final_url = "{:}?next={:}".format(reverse('account_login'), url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, final_url)

    @patch("stage_api.models.lta.celery_app.send_task")
    def test_restart_not_creator(self, st_mock):
        self.login_as_user()
        url = reverse("dashboard")

        response = self.client.get(url, {'to_details': 'true'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.view_name, 'dashboard')

    def restart_request(self):
        url = reverse("request-details-restart", args=[self.request.pk])
        final_url = reverse('request-details', args=[self.request.pk])
        response = self.client.get(url, {'to_details': 'true'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, final_url)
        request = LTARequest.objects.get(pk=self.request.pk)
        self.assertEqual(request.current_status_code, 'S')

    @patch("stage_api.models.lta.celery_app.send_task")
    def test_restart_creator(self, mock):
        self.login_as_creator()
        self.restart_request()

    @patch("stage_api.models.lta.celery_app.send_task")
    def test_restart_staff(self, mock):
        self.login_as_staff()
        self.restart_request()

    @patch("stage_api.models.lta.celery_app.send_task")
    def test_restart_admin(self, mock):
        self.login_as_admin()
        self.restart_request()
