from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import Dashboard, RequestDetails, UserProfile, InsertRequest, Statistics, InsertStatisticDashboard, \
    StatisticDashboard, claim_draft_request, restart_request, abort_request

conf = DefaultRouter()
conf.register('statistics_api', StatisticDashboard)

urlpatterns = [
    path('', Dashboard.as_view(), name='dashboard'),
    path('create', InsertRequest.as_view(), name='insert-request' ),
    path('requests/<int:pk>/', RequestDetails.as_view(), name='request-details'),
    path('requests/<int:pk>/restart/', restart_request, name='request-details-restart'),
    path('requests/<int:pk>/abort/', abort_request, name='request-details-abort'),
    path('draft-request/<str:pk>/claim/', claim_draft_request, name='draft-request-claim'),
    path('accounts/profile', UserProfile.as_view(), name='account-profile'),
    path('statistics/', Statistics.as_view(), name='statistics-dashboard'),
    path('statistics/insert', InsertStatisticDashboard.as_view(), name='insert-dashboard'),
    path('conf/', include(conf.urls)),
]
