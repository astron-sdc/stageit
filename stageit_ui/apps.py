from django.apps import AppConfig


class StageitUiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'stageit_ui'
