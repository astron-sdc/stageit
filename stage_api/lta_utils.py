import logging
logger = logging.getLogger(__name__)

try:
    import gfal2
except ModuleNotFoundError:
    from unittest.mock import MagicMock
    gfal2 = MagicMock()
    logger.warning('Missing gfal2 library')

import errno

from dcacheclient.rest import Context as RestContext, Locality
from typing import Union
import os
from typing import List
from django.conf import settings


_NO_SUCH_SPACE_TOKEN_ERROR = 53

Context = gfal2.Gfal2Context


class ContextConfig:
    path_to_proxy = None
    source = ''

    def __init__(self, path_to_proxy=None, source=''):
        self.source = source
        self.path_to_proxy = find_proxy(path_to_proxy)


def gfal_context(config: ContextConfig = None) -> Context:
    if config is None:
        config = ContextConfig()
    context = gfal2.creat_context()
    # NOTE currently not working
    # gfal2.cred_set(context, config.source, gfal2.cred_new("X509_KEY", config.path_to_proxy))
    # gfal2.cred_set(context, config.source, gfal2.cred_new("X509_CERT", config.path_to_proxy))
    return context


def rest_context(config: ContextConfig = None) -> Context:

    site_url = 'https://dcacheview.grid.surfsara.nl:22882'
    if config is None:
        config = ContextConfig()

    context = RestContext.create_from_x509(site_url, config.path_to_proxy)
    return context


def context_decorator(func):
    def wrapper(*args, config: ContextConfig = None, **kwargs):
        ctx = None
        try:
            if isinstance(Context, RestContext):
                ctx = rest_context(config)
            else:
                ctx = gfal_context(config)

            return func(ctx, *args, **kwargs)
        except Exception as e:
            raise e
        finally:
            if ctx and hasattr(ctx, 'free'):
                ctx.free()

    return wrapper


@context_decorator
def bring_online(ctx: Union[RestContext, gfal2.Gfal2Context], surls, pin_time, request_timeout, async_op=True):
    '''
    Bring online the files
    :param surl:
    :param pin_time:
    :param request_timeout:
    :param config:
    :return: status, token
    :raises:
        GError: standard error for staging issues
    '''
    status, token = ctx.bring_online(surls, pin_time, request_timeout, async_op)
    if not token:
        return status, ''
    else:
        return status, token


@context_decorator
def release(ctx: Union[RestContext, gfal2.Gfal2Context], surls, token):
    return ctx.release(surls, token)


@context_decorator
def poll_staging_request_status(ctx: Union[RestContext, gfal2.Gfal2Context], surls, token):
    return ctx.bring_online_poll(surls, token)


@context_decorator
def list_xattr(ctx: Union[RestContext, gfal2.Gfal2Context], path):
    return ctx.listxattr(path)


@context_decorator
def size_of_file(ctx: Context, path):
    stat = ctx.lstat(path)
    if stat and isinstance(stat, dict):
        return stat['st_size']
    elif stat:
        return stat.st_size
    else:
        return None


def evaluate_errors_bring_online(surls, in_errors):
    errors = {}
    progressing = []
    online = []

    n_progressing = 0
    n_finished = 0
    for surl, error in zip(surls, in_errors):
        if isinstance(error, str):
            if error == Locality.ONLINE_AND_NEARLINE.value:
                n_finished += 1
                online.append(surl)
            elif error == Locality.NEARLINE.value:
                n_progressing += 1
                progressing.append(surl)
        elif error and error.code == errno.EAGAIN:
            n_progressing += 1
            progressing.append(surl)
        elif error:
            errors[surl] = error.message
            n_finished += 1
        else:
            n_finished += 1
            online.append(surl)

    return {'errors': errors, 'progressing': progressing, 'online': online,
            'n_errors': len(errors), 'n_progressing': n_progressing, 'n_online': len(online), 'n_finished': n_finished }

@context_decorator
def stat(ctx: Context, path):
    try:
        stat = ctx.lstat(path)
    except gfal2.GError as e:
        logger.exception('error handling %s: %s', path, e)
        return None
    if stat and isinstance(stat, dict):
        stat_dict = {
            field.replace('st_', ''): stat[field]
            for field in (
                'st_atime', 'st_ctime', 'st_dev', 'st_gid', 'st_ino', 'st_mode', 'st_mtime', 'st_nlink', 'st_size',
                'st_uid')
        }
        return stat_dict
    elif stat and not isinstance(stat, dict):
        stat_dict = {
            field.replace('st_', ''): getattr(stat, field)
            for field in (
                'st_atime', 'st_ctime', 'st_dev', 'st_gid', 'st_ino', 'st_mode', 'st_mtime', 'st_nlink', 'st_size',
                'st_uid')
        }
        return stat_dict

    else:
        return None


@context_decorator
def stat_list(ctx: Union[RestContext, gfal2.Gfal2Context], path_list):
    return [stat(path) for path in path_list]


@context_decorator
def get_xattr(ctx: gfal2.Gfal2Context, surl, attr_name):
    return ctx.getxattr(ctx, surl, attr_name)


@context_decorator
def abort_staging_request(ctx: Context, surl, token):
    return ctx.abort_bring_online(surl, token)


def _request_all_xattrs(ctx: gfal2.Gfal2Context, surl: str):
    xattrs_keys = ctx.listxattr(surl)
    xattrs_dict = {}
    for key in xattrs_keys:
        try:
            xattrs_dict[key] = ctx.getxattr(surl, key)
        except gfal2.GError as e:
            if e.code == _NO_SUCH_SPACE_TOKEN_ERROR:
                xattrs_dict[key] = None
            else:
                raise e

    return xattrs_dict


@context_decorator
def xattrs(ctx: Context, surl: str):
    if isinstance(ctx, gfal2.Gfal2Context):
        return _request_all_xattrs(ctx, surl)
    else:
        return ctx.xattrs(surl)


@context_decorator
def xattrs_list(ctx: Context, surl_list: List[str]):
    if isinstance(ctx, gfal2.Gfal2Context):
        return [_request_all_xattrs(ctx, surl) for surl in surl_list]
    else:
        return [ctx.xattrs(surl) for surl in surl_list]


def find_proxy(path=None):

    proxy_by_name = os.environ.get('X509_USER_PROXY', '')
    proxy_by_default_path = '/tmp/x509up_u%s' % os.getuid()

    if path:
        if os.path.exists(path) and os.path.isfile(path):
            return path

    for expected_path in proxy_by_name, proxy_by_default_path:
        if expected_path and os.path.exists(expected_path) and os.path.isfile(expected_path):
            return expected_path
    else:
        raise FileNotFoundError('Missing voms proxy')
