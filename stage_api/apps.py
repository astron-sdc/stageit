from django.apps import AppConfig
from prometheus_client import REGISTRY
from stage_api.metrics import StageITCustomCollector
import sys

class StageApiConfig(AppConfig):
    name = 'stage_api'
    default_auto_field = 'django.db.models.AutoField'

    def ready(self):
        # Only register collector if we are not running migrations, because that will give trouble like
        # "psycopg2.errors.UndefinedTable: relation "stage_api_ltarequest" does not exist"
        # "LINE 1: SELECT COUNT(*) AS "__count" FROM "stage_api_ltarequest" WHE."
        # fix in a dirty but effective way
        if 'migrate' in sys.argv:
            return
        REGISTRY.register(StageITCustomCollector())