import logging
logger = logging.getLogger(__name__)
import json
import requests
from stage_api.lta_utils import find_proxy
from stage_api.models.lta import LTAMacaroon, LTASite

SPLITTER = "/projects"

class StageITConfigurationError(Exception):
    pass


def store_macaroon(lta_request, surls):
    """
    Store macaroon. For every surl of then LTARequest:
        If surl (LTASite) does not have a LTAMacaroon than
        1. Determine the root_caveat_path of the surl;
           This is the path after surl_pattern and before the <project_code>
           See "https://support.astron.nl/confluence/display/SDCP/Implementation + details  # Implementationdetails-Macaroons"
        2. Send POST request for macaroon using root_caveat_path
        3. Create LTAMacaroon object for request
    The macaroon is valid for 6 days (configurable per LTA Site)
    There is one exception to NOT generate macaroons! For LDV Staging service, the generation of macaroons is
    superfluous and should be skipped. Unfortunately the user is configured as admin (not LDV).
    It a kind of quick and dirty but if user is 'admin' than no macaroon will be created
    :param lta_request: The LTARequest object
    :param surls: List of surls of the LTARequest
    """
    if is_admin_user(lta_request):
        logging.info("No need to create macaroon")
        return

    for surl in surls:
        process_surl(lta_request, surl)


def is_admin_user(lta_request):
    """Check if the user is an admin."""
    return lta_request.user is not None and lta_request.user.username == "admin"


def process_surl(lta_request, surl):
    """Process each SURL and store macaroon if not already exist."""
    if lta_request.macaroon_exists_for_surl(surl):
        return

    logging.info(f"Generate macaroon for stage request {lta_request.id}")
    lta_site = LTASite.get_site_from_surl(surl)

    if lta_site:
        webdav_base_path = lta_site.webdav_base_url
        if not webdav_base_path:
            logging.info(
                f"No webdav_url defined for {lta_site.name}, skipping macaroon for request {lta_request.id} with {surl}")
        else:
            generate_and_store_macaroon(lta_request, surl, lta_site)
    else:
        raise StageITConfigurationError(f'{surl} does not have an associated LTA Site. Please configure properly')


def generate_and_store_macaroon(lta_request, surl, lta_site):
    """Generate and store the macaroon for the SURL and LTA site."""
    caveat_root_path = generate_caveat_root_path(surl, lta_site)
    proxy_file_path = find_proxy()
    verify_certificate = should_verify_certificate(lta_site)
    caveats = create_caveats(caveat_root_path, lta_site.valid_time)

    logging.info(f"Do a macaroon request at {lta_site.webdav_base_url} with caveats: {caveats} ...")
    response = send_macaroon_request(lta_site.webdav_base_url, caveats, proxy_file_path, verify_certificate)

    logging.info(f"...request for macaroon done with status_code {response.status_code}.")
    if response.ok:
        save_macaroon(response.json()['macaroon'], lta_request, caveat_root_path, lta_site)
    else:
        logging.error(f"Cannot save macaroon for request {lta_request.id} with {surl}")


def generate_caveat_root_path(surl, lta_site):
    """Generate the root caveat path based on the SURL and LTA site."""
    return surl.replace(lta_site.surl_pattern, "").split(SPLITTER, 1)[0] + SPLITTER


def should_verify_certificate(lta_site):
    """Determine whether to verify the certificate for the LTA site."""
    return not lta_site.disable_certificate_check


def create_caveats(caveat_root_path, valid_time):
    """Create caveats for the macaroon request."""
    return {
        "caveats": [f"root:{caveat_root_path}", "activity:DOWNLOAD"],
        "validity": f"P{valid_time}D"
    }


def send_macaroon_request(webdav_base_url, caveats, proxy_file_path, verify_certificate):
    """Send a macaroon request to the WebDAV base URL."""
    return requests.post(
        webdav_base_url,
        headers={'Content-Type': 'application/macaroon-request'},
        data=json.dumps(caveats),
        cert=proxy_file_path,
        verify=verify_certificate
    )


def save_macaroon(macaroon_content, lta_request, caveat_root_path, lta_site):
    """Save the generated macaroon to the database."""
    lta_macaroon = LTAMacaroon(
        requested_by=lta_request,
        content=macaroon_content,
        caveat_root_path=caveat_root_path,
        lta_site=lta_site
    )
    lta_macaroon.save()
    logging.info(f"Macaroon id={lta_macaroon.id} saved")

