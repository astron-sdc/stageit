import rest_framework.request
from django.shortcuts import redirect
from django.views.decorators.http import require_GET
from graphene_django.views import GraphQLView
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes, authentication_classes, action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView

from stage_api.models.draft import DraftLTARequest
from stage_api.models.lta import LTASite, LTARequest, LTAError, LTAMacaroon
from stage_api.serializers import LTASiteSerializer, LTARequestSerializer, LTAErrorSerializer, DraftLTARequestSerializer, LTAMacaroonSerializer
from stage_api.tasks import ping, abort_staging_request
from stage_api.utils import srm_to_gridftp, srm_to_webdav, get_retried_request_ids_per_lta_site, \
    get_total_surl_size_and_retry_count_per_lta_site, get_total_surl_size_and_retry_count_per_user,\
    get_total_surl_size_per_retry_count_per_lta_site
from prometheus_client import generate_latest, REGISTRY
from django.http import HttpResponse, JsonResponse


@require_GET
@api_view(['GET'])
def async_ping(request, format=None):
    response = ping.delay()
    return Response(response.get(timeout=10), status=status.HTTP_200_OK)

@require_GET
@api_view(['GET'])
def convert_surls_to_webdav(request, pk):
    ltarequest = LTARequest.objects.get(pk=pk)
    surls = list(ltarequest.surls.values_list('surl', flat=True))
    webdav_urls = [srm_to_webdav(surl, ltarequest) for surl in surls]
    return Response(webdav_urls, status=200)


@require_GET
@api_view(['GET'])
def convert_surls_to_gridftp(request, pk):
    ltarequest = LTARequest.objects.get(pk=pk)
    surls = list(ltarequest.surls.values_list('surl', flat=True))
    gridftp_urls = [srm_to_gridftp(surl) for surl in surls]
    return Response(gridftp_urls, status=200)


class StagingRequestViewset(ModelViewSet):
    serializer_class = LTARequestSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        current_user = self.request.user
        if current_user.is_staff:
            return LTARequest.objects.all().order_by('-id')
        else:
            return LTARequest.objects.filter(user=current_user)

    def create(self, request, *args, **kwargs):
        request.data['user'] = self.request.user.pk
        return super().create(request, *args, **kwargs)

    def delete(self, request, pk, format=None):
        abort_staging_request.delay(pk)
        return Response(status=202)

    @action(detail=True, methods=['put'])
    def abort(self, request: Request, pk=None):

        lta_request: LTARequest = self.get_object()
        if lta_request.can_be_aborted():
            lta_request.abort(request.user)
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED, data={'message': 'invalid status of lta request',
                                                                             'lta_request_status': lta_request.current_status_code})


class LTAErrorViewset(ModelViewSet):
    queryset = LTAError.objects.all()
    serializer_class = LTAErrorSerializer
    permission_classes = [IsAdminUser]


class LTASiteViewset(ModelViewSet):
    queryset = LTASite.objects.all()
    serializer_class = LTASiteSerializer
    permission_classes = [IsAdminUser]


class LTAMacaroonViewset(ModelViewSet):
    queryset = LTAMacaroon.objects.all()
    serializer_class = LTAMacaroonSerializer
    permission_classes = [IsAdminUser]


class DraftLTARequestViewset(ModelViewSet):
    queryset = DraftLTARequest.objects.filter()
    serializer_class = DraftLTARequestSerializer
    permission_classes = [IsAdminUser]

    @action(detail=True, methods=['get'], permission_classes=[IsAuthenticated])
    def claim(self, request, pk=None):
        """Claims a DraftLTARequest, converting it to a regular request

        TODO: Using an HTTP GET for changing server state is bad practice.
        To get a post from an HTML template (such as the LTA GUI), you can
        use an HTML Form:
        ```html
        <form action="{draft request url}" method="post">
            <input type=submit>claim</input>
        </form>
        ```
        """
        user = request.user
        draft_request: DraftLTARequest = self.get_object()
        request = draft_request.create_request(user.pk)
        return redirect('LTARequest-detail', request.pk)


class GenerateToken(ObtainAuthToken):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user = request.user
        token, _ = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, })


class DRFAuthenticatedGraphQLView(GraphQLView):
    def parse_body(self, request):
        if isinstance(request, rest_framework.request.Request):
            return request.data
        return super(DRFAuthenticatedGraphQLView, self).parse_body(request)

    @classmethod
    def as_view(cls, *args, **kwargs):
        view = super(DRFAuthenticatedGraphQLView, cls).as_view(*args, **kwargs)
        view = permission_classes((IsAuthenticated,))(view)
        view = authentication_classes(api_settings.DEFAULT_AUTHENTICATION_CLASSES)(view)
        view = api_view(["GET", "POST"])(view)
        return view


@require_GET
@api_view(['GET'])
def stageit_metrics_view(request):
    """
    Show prometheus metrics endpoint
    Normally you get it for free because django_prometheus is installed BUT we want to add additional
    metrics for the stageit application. Which is created in the StageITCustomCollector
    See https://prometheus.github.io/client_python/collector/custom/
    """
    metrics_data = generate_latest(REGISTRY)
    return HttpResponse(metrics_data, content_type='text/plain')

@require_GET
@api_view(['GET'])
def stageit_get_lta_site_combined_info_view(request):
    """Just for test purposes"""
    user_id_info = get_total_surl_size_and_retry_count_per_user()
    lta_site_info = get_total_surl_size_and_retry_count_per_lta_site()
    retry_request_ids_info = get_retried_request_ids_per_lta_site()
    file_info_per_retry_count = get_total_surl_size_per_retry_count_per_lta_site()

    return JsonResponse({'file size/count/retry count per user id': user_id_info,
                         'file size/count/retry count per lta site': lta_site_info,
                         'retry request ids per lta site': retry_request_ids_info,
                         'file size/count per retry count': file_info_per_retry_count})
