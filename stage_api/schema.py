import graphene
import graphene_django_optimizer as gql_optimizer
from django_filters import FilterSet, OrderingFilter
from graphene import relay
from graphene_django import DjangoObjectType, DjangoListField
from graphene_django.filter import DjangoFilterConnectionField
from django.db.models import Field

from stage_api.lookups.not_equal import NotEqual
from stage_api.models.lta import LTARequest, AffectedSurls, LTAMacaroon, LTASite

Field.register_lookup(NotEqual)


class AffectedSurlsNode(DjangoObjectType):
    class Meta:
        model = AffectedSurls
        fields = ['surl']


class LTASiteNode(DjangoObjectType):
    class Meta:
        model = LTASite
        fields = ['name']  # Add other fields if needed


class LTAMacaroonNode(DjangoObjectType):
    lta_site = graphene.Field(LTASiteNode)

    class Meta:
        model = LTAMacaroon
        fields = ['content', 'valid_until', 'lta_site']

    def resolve_lta_site(self, info):
        return self.lta_site


class LTARequestNode(DjangoObjectType):
    id = graphene.ID(source='pk', required=True)
    surls = DjangoListField(AffectedSurlsNode)
    current_status = graphene.String(source='current_status')
    macaroons = DjangoListField(LTAMacaroonNode)

    @gql_optimizer.resolver_hints(
        model_field='current_status_code'
    )
    def resolve_current_status_code(root, info):
        return root.current_status_code

    @gql_optimizer.resolver_hints(
        model_field='request_type'
    )
    def resolve_request_type(root, info):
        return root.request_type

    class Meta:
        model = LTARequest
        fields = ['id', 'request_token', 'request_type', 'when', 'response', 'current_status', 'current_status_code']
        interfaces = (relay.Node, )

    @classmethod
    def get_queryset(cls, queryset, info):
        current_user = info.context.user

        if not current_user.is_authenticated:
            return LTARequest.objects.none()

        qs = LTARequest.objects.all() if current_user.is_staff else LTARequest.objects.filter(user=current_user)

        return gql_optimizer.query(qs, info, disable_abort_only=True)


class LTARequestFilter(FilterSet):
    class Meta:
        model = LTARequest
        fields = {
            'current_status_code': ['exact', 'in', 'ne'],
            'request_type': ['exact'],
            'when': ['lt', 'lte', 'gt', 'gte']
        }

    order_by = OrderingFilter(
        fields=(
            ('id', 'request_type', 'when', 'current_status_code')
        )
    )


class Query(graphene.ObjectType):
    requests = DjangoFilterConnectionField(LTARequestNode, filterset_class=LTARequestFilter)
    request = graphene.Field(LTARequestNode, id=graphene.Int())
    all_macaroons = graphene.List(LTAMacaroonNode)

    def resolve_request(root, info, id):
        queryset = LTARequestNode.get_queryset(None, info)
        return queryset.get(pk=id)

    def resolve_all_macaroons(self, info):
        return LTAMacaroon.objects.all()

schema = graphene.Schema(query=Query)
