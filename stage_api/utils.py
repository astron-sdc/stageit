from django.db.models import Count, Sum, Subquery, OuterRef, IntegerField, Q
from stage_api.models.lta import LTASite, LTARequest, LTARequestType
from collections import defaultdict


def srm_to_download(surl):
    return LTASite.get_site_from_surl(surl).get_download_path(surl)


def srm_to_gridftp(surl):
    return LTASite.get_site_from_surl(surl).get_gridftp_path(surl)


def srm_to_webdav(surl, lta_request):
    lta_site = LTASite.get_site_from_surl(surl)
    macaroon = lta_request.get_macaroon_from_site(lta_site)
    if macaroon:
        webdav_path = surl.replace(lta_site.surl_pattern, lta_site.webdav_base_url)
        return webdav_path.replace(macaroon.caveat_root_path, "")
    else:
        return ""


def defaultdict_to_dict(d):
    """
    Function to recursively convert defaultdict to dict
    """
    if isinstance(d, defaultdict):
        d = {k: defaultdict_to_dict(v) for k, v in d.items()}
    return d


def get_retried_request_ids_per_lta_site():
    """
    Retrieve the staging request ids per lta site per lta retry counter
    return: dictionary like
     {<lta site name 1>: 1: {[<req_id>, ...] },
                         2: {[<req_id>, ...] },
                         3: {[<req_id>, ...] }}
     {<lta site name 2>: 1: {[<req_id>, ...] },
                         2: {[<req_id>, ...] },
                         3: {[<req_id>, ...] }},
                         ...
     """
    # Initialize the defaultdict -> https://docs.python.org/3/library/collections.html#defaultdict-examples
    site_retry_request_ids = defaultdict(lambda: defaultdict(list))
    # Fetch LTARequest objects with retry_count > 1 and prefetch related LTASites
    lta_requests = LTARequest.objects.prefetch_related('lta_sites').filter(retry_count__gt=0,
                                                                           request_type=LTARequestType.STAGE)
    # Iterate over each request and its related LTASites
    for request in lta_requests:
        for site in request.lta_sites.all():
            site_retry_request_ids[site.name][request.retry_count].append(request.id)
    # Convert the nested defaultdict to a regular dict
    return defaultdict_to_dict(site_retry_request_ids)


def get_total_surl_size_per_retry_count_per_lta_site():
    """
    Retrieve the total surls and total size per retry_count per lta site
    return: dictionary like
    {'<lta site name 1>': {1: {'total_surls': <count>, 'total_size': <sum>, 'total_request_ids': <count>},
                           2: {'total_surls': <count>, 'total_size': <sum>, 'total_request_ids': <count>},
                           3: {'total_surls': <count>, 'total_size': <sum>, 'total_request_ids': <count>}},
     '<lta site name 2>': {1: {'total_surls': <count>, 'total_size': <sum>, 'total_request_ids': <count>},
                           2: {'total_surls': <count>, 'total_size': <sum>, 'total_request_ids': <count>},
                           3: {'total_surls': <count>, 'total_size': <sum>, 'total_request_ids': <count>}}}
    """
    result = {}

    # Aggregating total surls, total size, total retries and total request ids grouped by site ID and retry count
    combined_info = LTARequest.objects.filter(retry_count__gt=0, request_type=LTARequestType.STAGE
    ).values(
        'lta_sites__name', 'retry_count'
    ).annotate(
        total_surls=Count('surls'),
        total_size=Sum('surls__size'),
        total_request_ids = Count('id', distinct=True)
    ).order_by('lta_sites__name', 'retry_count')

    for entry in combined_info:
        site_name = entry['lta_sites__name']
        retry_count = entry['retry_count']

        if site_name not in result:
            result[site_name] = {}
        if retry_count not in result[site_name]:
            result[site_name][retry_count] = {
                "total_surls": 0,
                "total_size": 0,
                "total_request_ids": 0
            }

        result[site_name][retry_count]["total_surls"] += entry['total_surls']
        result[site_name][retry_count]["total_size"] += entry['total_size']
        result[site_name][retry_count]["total_request_ids"] += entry['total_request_ids']

    return result


def get_total_surl_size_and_retry_count_per_lta_site():
    """
    Retrieve the total surls, total size and total retry_count per lta site
    return: dictionary like
    {'<lta site name 1>': {'total_surls': <count>, 'total_size': <sum>, 'total_retries': <sum>, 'total_request_ids': <count>},
     '<lta site name 2>': {'total_surls': <count>, 'total_size': <sum>, 'total_retries': <sum>, 'total_request_ids': <count>}}
    """
    # Subquery for total retries per site
    # There was an issue with the number of retry counts (looked double values were retrurned)
    # It seems that the issue might be caused by the way the joins are being made in the query.
    # When you join LTARequest and AffectedSurls in the same query, it might create a Cartesian product,
    # leading to duplications in counts and sums. To resolve this,
    # you can use subqueries to separately compute the totals and then combine them.
    # First, ensure you import Subquery and OuterRef:
    # Then, create a subquery for total_retries to avoid the Cartesian product issue
    # Explanation
    # total_retries_subquery: This subquery calculates the total retries for each LTASite by summing the retry_count
    # of related LTARequest objects. It avoids the Cartesian product by focusing only on the retry_count aggregation.
    # Subquery: This is used to integrate the result of the subquery back into the main query.
    result = {}
    total_retries_subquery = LTARequest.objects.filter(
        lta_sites=OuterRef('pk'),
        request_type=LTARequestType.STAGE
    ).values('lta_sites').annotate(
        total_retries=Sum('retry_count')
    ).values('total_retries')

    # Main query with annotations
    combined_info = LTASite.objects.annotate(
        total_surls=Count('ltarequest__surls', filter=Q(ltarequest__request_type=LTARequestType.STAGE)),
        total_size=Sum('ltarequest__surls__size', filter=Q(ltarequest__request_type=LTARequestType.STAGE)),
        total_retries=Subquery(total_retries_subquery, output_field=IntegerField()),
        total_request_ids=Count('ltarequest__id', filter=Q(ltarequest__request_type=LTARequestType.STAGE), distinct=True)
    ).values('name', 'total_surls', 'total_size', 'total_retries', 'total_request_ids')
    # Transform the queryset result into the desired dictionary format
    for site in combined_info:
        result[site['name']] = {
            'total_surls': site['total_surls'],
            'total_size': site['total_size'],
            'total_retries': site['total_retries'],
            'total_request_ids': site['total_request_ids']}

    return result


def get_total_surl_size_and_retry_count_per_user():
    """
    Retrieve the total surls, total size, and total retry count per user.
    return: dictionary like
    {'<user_id 1>': {'total_surls': <count>, 'total_size': <sum>, 'total_retries': <sum>, 'total_request_ids': <sum>},
     '<user_id 2>': {'total_surls': <count>, 'total_size': <sum>, 'total_retries': <sum>, 'total_request_ids': <sum>}}
    """
    result = {}

    # Aggregating total surls, total size, and total retries grouped by user ID
    combined_info = LTARequest.objects.filter(retry_count__gt=0, request_type=LTARequestType.STAGE
    ).values(
        'user_id'
    ).annotate(
        total_surls=Count('surls'),
        total_size=Sum('surls__size'),
        total_retries=Sum('retry_count'),
        total_request_ids=Count('id', distinct=True)
    ).order_by('user_id')

    for entry in combined_info:
        user_id = entry['user_id']

        if user_id not in result:
            result[user_id] = {
                "total_surls": 0,
                "total_size": 0,
                "total_retries": 0,
                "total_request_ids": 0
            }
        result[user_id]["total_surls"] += entry['total_surls']
        result[user_id]["total_size"] += entry['total_size']
        result[user_id]["total_retries"] += entry['total_retries']
        result[user_id]["total_request_ids"] += entry['total_request_ids']

    return result




