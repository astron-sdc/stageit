from django import template
from stage_api.utils import srm_to_download as srm_to_download_util
from stage_api.utils import srm_to_gridftp as srm_to_gridftp_util
from stage_api.utils import srm_to_webdav as srm_to_webdav_util

register = template.Library()


@register.filter
def srm_to_download(surl):
    return srm_to_download_util(surl)


@register.filter
def srm_to_gridftp(surl):
    return srm_to_gridftp_util(surl)


@register.simple_tag
def srm_to_webdav(surl, lta_request):
    return srm_to_webdav_util(surl, lta_request)

