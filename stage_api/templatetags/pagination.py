from django import template
from urllib.parse import quote
register = template.Library()

@register.filter
def to_page(request, page_num):
    """
    Use this filter tag to navigate the pagination urls keeping intact the query string
    """
    next_url = ''
    for key, value in request.GET.items():
        if key == 'page':
            continue
        next_url += f'{key}={quote(value)}&'

    next_url = f'?{next_url}page={page_num}'
    return next_url
