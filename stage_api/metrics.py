from prometheus_client.core import GaugeMetricFamily, CounterMetricFamily, REGISTRY
from prometheus_client.registry import Collector


class StageITCustomCollector(Collector):
    def collect(self):
        yield self.create_lta_stage_requests_gauge()
        yield self.create_lta_site_files_gauge()
        yield self.create_lta_user_files_gauge()
        yield self.create_lta_retry_numbers_gauge()

    @staticmethod
    def create_lta_stage_requests_gauge():
        """
           Create the lta_stage_requests_gauge metrics
           This metrics count the stage requests per status
        """
        # need to import here otherwise you get "AppRegistryNotReady: Apps aren't loaded yet"
        from stage_api.models.lta import LTARequest, LTARequestStatusCode, LTARequestType

        gauge = GaugeMetricFamily(
            name='stageit_lta_stage_requests_active',
            documentation='Number of active LTA stage requests per status',
            labels=['status_code', 'status_text'])

        for status_code, status_text in LTARequestStatusCode.choices:
            count = LTARequest.objects.filter(current_status_code=status_code,
                                              request_type=LTARequestType.STAGE).count()
            gauge.add_metric([status_code, status_text], count)
        return gauge

    @staticmethod
    def create_lta_site_files_gauge():
        """
           Update the lta_site_files_gauge metrics
           This metrics count the retries, number of files and total size per LTA site
        """
        # need to import here otherwise you get "AppRegistryNotReady: Apps aren't loaded yet"
        from stage_api.utils import get_total_surl_size_and_retry_count_per_lta_site

        gauge = GaugeMetricFamily(
            name='stageit_lta_site_total_scheduled_files',
            documentation='Total number of requests, scheduled files, files sizes and retries per LTA site',
            labels=['lta_site', 'type'])

        combined_info = get_total_surl_size_and_retry_count_per_lta_site()
        for lta_site, lta_info in combined_info.items():
            for key, value in lta_info.items():
                gauge.add_metric([lta_site, key], value or 0)
        return gauge

    @staticmethod
    def create_lta_user_files_gauge():
        """
           Update the lta_user_files_gauge metrics
           This metrics count the retries, number of files, total size, total request per User
        """
        # need to import here otherwise you get "AppRegistryNotReady: Apps aren't loaded yet"
        from stage_api.utils import get_total_surl_size_and_retry_count_per_user

        gauge = GaugeMetricFamily(
            name='stageit_lta_user_total_scheduled_files',
            documentation='Total number of requests, scheduled files, files sizes and retries per User ID',
            labels=['user', 'type'])

        combined_info = get_total_surl_size_and_retry_count_per_user()
        for user_id, user_info in combined_info.items():
            for key, value in user_info.items():
                # make sure user_id is string (labels must be stings !)
                gauge.add_metric([str(user_id), key], value or 0)
        return gauge

    @staticmethod
    def create_lta_retry_numbers_gauge():
        """
           Update the lta_retry_numbers_gauge metrics
           This metrics count the number of files and total size per retry_count (>0) per LTA site
        """
        # need to import here otherwise you get "AppRegistryNotReady: Apps aren't loaded yet"
        from stage_api.utils import get_total_surl_size_per_retry_count_per_lta_site

        gauge = GaugeMetricFamily(
            name='stageit_lta_retry_numbers',
            documentation='File count or file size per retry number per LTA site',
            labels=['lta_site', 'retry_count', 'type'])

        combined_info = get_total_surl_size_per_retry_count_per_lta_site()
        for lta_site, info_per_site in combined_info.items():
            for retry_count, info_per_retry in info_per_site.items():
                for key, value in info_per_retry.items():
                    # make sure retry_count is string (labels must be stings !)
                    gauge.add_metric([lta_site, str(retry_count), key], value or 0)
        return gauge


