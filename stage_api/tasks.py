import logging
from abc import ABC

from celery import shared_task, Task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.db.models import ObjectDoesNotExist

import stage_api.lta_utils as lta_utils
from stage_api.macaroon import store_macaroon
from stage_api.models.email import EmailTemplate
from stage_api.models.lta import LTASite, LTAError, LTARequest, LTARequestStatus, LTARequestStatusCode, LTARequestType


logger = get_task_logger(__name__)
ONE_DAY_IN_SECONDS = 3600 * 24


@shared_task(bind=True)
def ping():
    print('pong')
    return 'pong'


class TaskWithErrorHandling(Task, ABC):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        try:
            request_id, *_ = args

            lta_request = LTARequest.objects.get(pk=request_id)
            first_surl = lta_request.surls.first().surl
            lta_site = LTASite.get_site_from_surl(first_surl)

            if lta_request.retry_count >= settings.STAGEIT_MAX_RETRIES or lta_request.request_type != LTARequestType.STAGE:
                LTAError.objects.create(generated_by=lta_request, lta_site=lta_site, error=repr(exc),
                                        error_type=str(type(exc)))
            else:
                lta_request.retry_in_place()

            logger.exception('raised exception handling %s: %s', request_id, exc)
        except Exception as e:
            logger.exception('Failed to handle exception gracefully for task_id %s with args %s: %s', task_id, args, e)

        super().on_failure(exc, task_id, args, kwargs, einfo)

    def on_success(self, retval, task_id, args, kwargs):
        try:
            request_id, *_ = args
        except Exception as e:
            logger.exception('Failed to handle exception gracefully for task_id %s with args %s: %s', task_id, args, e)
        super().on_success(retval, task_id, args, kwargs)


class TaskWithErrorHandlingAndNotify(TaskWithErrorHandling, ABC):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        try:
            request_id, *_ = args
            super().on_failure(exc, task_id, args, kwargs, einfo)
            notify_user.delay(request_id)

        except Exception as e:
            logger.exception('Failed to handle exception gracefully for task_id %s with args %s: %s', task_id, args, e)

    def on_success(self, retval, task_id, args, kwargs):
        try:
            request_id, *_ = args
            notify_user.delay(request_id)
        except Exception as e:
            logger.exception('Failed to handle exception gracefully for task_id %s with args %s: %s', task_id, args, e)

        super().on_success(retval, task_id, args, kwargs)


@shared_task(bind=True, base=TaskWithErrorHandling)
def handle_status_change(self, lta_request: int, request_type, from_status, to_status):
    lta_request = LTARequest.objects.get(pk=lta_request)

    if lta_request.notify:
        try:
            email_template = EmailTemplate.objects.get(request_type=request_type, transition_from_status=from_status,
                                                       transition_to_status=to_status)

            if to_status == LTARequestStatusCode.ERROR and lta_request.retry_count == settings.STAGEIT_MAX_RETRIES:
                # Only send error mails after configured amount of retries
                email_template.send(lta_request)

            elif to_status == LTARequestStatusCode.COMPLETED or to_status == LTARequestStatusCode.INCOMPLETE:
                # Always send an update on completed tasks
                email_template.send(lta_request)

            elif to_status != LTARequestStatusCode.ERROR and lta_request.retry_count == 0:
                # Other updates only on first invocation
                email_template.send(lta_request)

            else:
                logging.debug('skipping email sending for task %s transitioning to %s: retyring failed', lta_request,
                              to_status)

        except ObjectDoesNotExist:
            logging.debug('skipping email sending for task %s transitioning to %s: missing email template', lta_request,
                          to_status)
    else:
        logging.debug('Notification are turned off for %s: skipping email sending on transition', lta_request)


def is_task_is_already_running(task):
    inspect = task.app.control.inspect()
    inspect_status = [inspect.scheduled(), inspect.active()]
    for statuses in inspect_status:
        for worker_name, worker in statuses.items():
            for job in worker:
                if job["id"] != task.request.id and job["name"] == task.name:
                    return True

    return False


@shared_task(bind=True)
def check_status(self):
    """
    Check the status of staging requests
    :param self: the task object
    :return:
    """
    if is_task_is_already_running(self):
        return

    logging.debug('task %s is handling checking pending requests', self.request.id)
    lta_requests_submitted = LTARequest.objects.filter(current_status_code=LTARequestStatusCode.SUBMITTED)
    lta_requests_stage_running = LTARequest.objects.filter(current_status_code=LTARequestStatusCode.RUNNING,
                                                           request_type=LTARequestType.STAGE)

    for lta_request in lta_requests_submitted:
        lta_status = LTARequestStatus(request=lta_request, status='Processing request',
                                      status_code=LTARequestStatusCode.PENDING)
        lta_status.save()
        process_request.delay(lta_request.pk)

    for lta_request in lta_requests_stage_running:
        check_stage_request_status(lta_request.pk)


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def check_stage_request_status(self, request_id):
    """
    Check the status of a stage request
    :param self: task object
    :param request_id: the id of the request to process
    :return:
    """
    request = LTARequest.objects.get(pk=request_id)
    surls = list(request.surls.values_list('surl', flat=True))
    token = request.request_token
    logging.info(f'request_id: {request_id} - request token: {token}')
    if token:
        errors = lta_utils.poll_staging_request_status(surls, token)
        current_status = lta_utils.evaluate_errors_bring_online(surls, errors)
        request.response = current_status
        if current_status['n_finished'] == len(surls) and current_status['n_errors'] == 0:
            logging.info(
                f'stage request {request.id} was set to completed, n_finished={current_status["n_finished"]}, len(surls)={len(surls)}, len(errors)={len(errors)}')
            status = LTARequestStatus(request=request, status='success', status_code=LTARequestStatusCode.COMPLETED)
            status.save()
            notify_user.delay(request_id)
        elif current_status['n_finished'] == len(surls):
            status = LTARequestStatus(request=request, status='completed with errors',
                                      status_code=LTARequestStatusCode.INCOMPLETE)
            status.save()
            notify_user.delay(request_id)
    else:
        logging.info(f'stage request {request.id} was set to completed, because the request token is missing')
        status = LTARequestStatus(request=request, status='completed with missing token',
                                  status_code=LTARequestStatusCode.COMPLETED)
        status.save()

    request.save()


@shared_task(bind=True, base=TaskWithErrorHandling)
def notify_user(self, request_id):
    """
    Notify user of a staging request status toggle
    :param self: task object
    :param request_id: the id of the request to process
    :return:
    """
    logging.info('task %s is handling request %s', self.request.id, request_id)
    pass


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def xattr_given_surl(self, request_id):
    """
    Extended attributes for the surl contained in the request id
    :param self: task object
    :param request_id: the id of the request to process
    :return:
    """
    logging.debug('task %s is handling request %s', self.request.id, request_id)
    request = LTARequest.objects.get(pk=request_id)
    if request.current_status_code in [LTARequestStatusCode.SUBMITTED, LTARequestStatusCode.PENDING]:
        lta_status = LTARequestStatus(request=request, status='Request in progress',
                                      status_code=LTARequestStatusCode.RUNNING)
        lta_status.save()
        surls = list(request.surls.values_list('surl', flat=True))
        xattrs = lta_utils.xattrs_list(surls)
        request.response['xattrs'] = xattrs
        logger.debug('request id %s has xattrs %s', request_id, xattrs)
        request.save(update_fields=['response'])

        lta_status = LTARequestStatus(request=request, status='Results ready',
                                      status_code=LTARequestStatusCode.COMPLETED)
        lta_status.save()


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def size_of_file(self, request_id):
    """
    Extended attributes for the surl contained in the request id
    :param request_id: the id of the request to process
    :return:
    """
    logging.debug('task %s is handling request %s', self.request.id, request_id)
    request = LTARequest.objects.get(pk=request_id)

    if request.current_status_code in [LTARequestStatusCode.SUBMITTED, LTARequestStatusCode.PENDING]:
        lta_status = LTARequestStatus(request=request, status='Request in progress',
                                      status_code=LTARequestStatusCode.RUNNING)
        lta_status.save()
        surls = list(request.surls.values_list('surl', flat=True))
        size_per_url = dict()
        for surl in surls:
            size_in_bytes = lta_utils.size_of_file(surl)
            size_per_url[surl] = size_in_bytes

        request.response['size'] = size_per_url
        request.save(update_fields=['response'])

        lta_status = LTARequestStatus(request=request, status='Results ready',
                                      status_code=LTARequestStatusCode.COMPLETED)
        lta_status.save()


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def stat(self, request_id):
    """
    Extended attributes for the surl contained in the request id
    :param request_id: the id of the request to process
    :return:
    """
    logging.debug('task %s is handling request %s', self.request.id, request_id)
    request = LTARequest.objects.get(pk=request_id)

    if request.current_status_code in [LTARequestStatusCode.SUBMITTED, LTARequestStatusCode.PENDING]:
        lta_status = LTARequestStatus(request=request, status='Request in progress',
                                      status_code=LTARequestStatusCode.RUNNING)
        lta_status.save()
        surls = list(request.surls.values_list('surl', flat=True))
        stats = dict()
        for surl in surls:
            stat = lta_utils.stat(surl)
            stats[surl] = stat

        request.response['stats'] = stats
        request.save(update_fields=['response'])

        lta_status = LTARequestStatus(request=request, status='Results ready',
                                      status_code=LTARequestStatusCode.COMPLETED)
        lta_status.save()


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def stage_surl_list(self, request_id):
    """
    Stage the current list of SURLs
    :param request_id: the id of the request to process
    :return:
    """
    request = LTARequest.objects.get(pk=request_id)
    logging.info('task %s is handling request %s', self.request.id, request_id)

    pintime = request.pintime
    surls = list(request.surls.values_list('surl', flat=True))
    status, token = lta_utils.bring_online(surls, pintime, 3 * ONE_DAY_IN_SECONDS)
    logging.info('stage request %s returned: %s, %s', self.request.id, status, token)
    request.request_token = token
    request.save(update_fields=['request_token'])
    # Create a macaroon
    store_macaroon(request, surls)

    # setting the request to status RUNNING will trigger the status check, which assumes that the request_token has been saved on the request
    # do this only after saving the request_token to prevent timing problems
    lta_status = LTARequestStatus(request=request, status='Processing request',
                                  status_code=LTARequestStatusCode.RUNNING)
    lta_status.save()

    # update surl sizes asynchronously
    update_surl_sizes.delay(request_id)

    if token == "":
        request.response = {'errors': [], 'progressing': [], 'online': surls, 'n_errors': 0, 'n_progressing': 0,
                            'n_online': len(surls), 'n_finished': len(surls)}
        request.save(update_fields=['response'])
        lta_status = LTARequestStatus(request=request, status='Files are online',
                                      status_code=LTARequestStatusCode.COMPLETED)
        lta_status.save()

    else:
        request.request_token = token
        request.save(update_fields=['request_token'])
        lta_status = LTARequestStatus(request=request, status='Request sent to LTA sites',
                                      status_code=LTARequestStatusCode.RUNNING)
        lta_status.save()


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def update_surl_sizes(self, request_id):
    request = LTARequest.objects.get(pk=request_id)

    aff_surls = request.surls.all()
    logging.info(f'Updating size of {len(aff_surls)} affected surls')
    for aff_surl in aff_surls:
        aff_surl.update_size()


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def abort_staging_request(self, request_id):
    """
    Abort the staging request
    :param request_id: the id of the request to process
    :return:
    """
    logging.debug('task %s is handling request %s', self.request.id, request_id)
    request = LTARequest.objects.get(pk=request_id)
    token = request.request_token
    if token:
        surls = list(request.surls.values_list('surl', flat=True))
        result = lta_utils.abort_staging_request(surls, token)
        logging.debug('result from aborting request %s is: %s', request_id, result)
        lta_status = LTARequestStatus(status='Aborted', status_code=LTARequestStatusCode.COMPLETED, request=request)
        lta_status.save()
    else:
        lta_status = LTARequestStatus(status='Missing Token', status_code=LTARequestStatusCode.ERROR, request=request)
        lta_status.save()


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def release_pin(self, request_id):
    """
    Abort the staging request
    :param request_id: the id of the request to process
    :return:
    """
    logging.debug('task %s is handling request %s', self.request.id, request_id)
    request = LTARequest.objects.get(pk=request_id)
    token = request.request_token
    if token is not None:
        surls = list(request.surls.values_list('surl', flat=True))
        result = lta_utils.release(surls, token)
        logging.debug('result from aborting request %s is: %s', request_id, result)
        lta_status = LTARequestStatus(status='Released', status_code=LTARequestStatusCode.COMPLETED, request=request)
        lta_status.save()
    else:
        lta_status = LTARequestStatus(status='Missing Token', status_code=LTARequestStatusCode.ERROR, request=request)
        lta_status.save()


__REQUEST_TYPE_TO_METHOD = {LTARequestType.STAGE: stage_surl_list, LTARequestType.XATTRS: xattr_given_surl,
                            LTARequestType.SIZE: size_of_file, LTARequestType.STATS: stat,
                            LTARequestType.RELEASE: release_pin, LTARequestType.ABORT: abort_staging_request}


@shared_task(bind=True, base=TaskWithErrorHandlingAndNotify)
def process_request(self, request_id):
    logging.debug('task %s is handling request %s', self.request.id, request_id)
    request = LTARequest.objects.get(pk=request_id)
    __REQUEST_TYPE_TO_METHOD[request.request_type].delay(request_id)



