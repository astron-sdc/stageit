from typing import List, Union, Tuple

from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

from stage_api.models.user_info import UserInfo, ProjectRole


def parse_entitlement_rule(entitlement_rule: str) -> Union[None, Tuple[str, ProjectRole]]:
    if 'lofar:project:' in entitlement_rule and ':role=' in entitlement_rule:
        project, role = entitlement_rule.split('lofar:project:')[1].split(':role=')
        return project, ProjectRole(project=project, roles=[role])
    elif 'lofar:project:' in entitlement_rule:
        project = entitlement_rule.split('lofar:project:')[1]
        return project, ProjectRole(project=project)
    return None


def parse_entitlements_into_project_roles(entitlements: Union[List[str], None]) -> List[ProjectRole]:
    project_roles = {}
    if entitlements is None:
        return list()
    for entitlement_rule in entitlements:
        parsed = parse_entitlement_rule(entitlement_rule)
        if parsed:
            project, value = parsed
            if project in project_roles:
                project_roles[project].add_role(value)
            else:
                project_roles[project] = value
    return list(project_roles.values())


class SocialAccountAdapter(DefaultSocialAccountAdapter):

    def save_user(self, request, sociallogin, form=None):
        """Hook called when a user is saved"""
        user = super().save_user(request, sociallogin, form)

        userinfo, _ = UserInfo.objects.update_or_create(user=user, location=sociallogin.account.extra_data.get('l'))
        project_roles: List[ProjectRole] = parse_entitlements_into_project_roles(
            sociallogin.account.extra_data.get('eduperson_entitlement'))
        for project_role in project_roles:
            project_role.user_info = userinfo
            project_role.save()

        return user

    def authentication_error(self, request, provider_id, error=None, exception=None, extra_context=None, ):
        print('SocialAccount authentication error!', 'error', request,
              {'provider_id': provider_id, 'error': error.__str__(), 'exception': exception.__str__(),
               'extra_context': extra_context})
