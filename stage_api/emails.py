import dataclasses

from django.core.mail import send_mail, EmailMessage
from django.template import Context, Template
from typing import List
FROM_EMAIL = 'stageit-noreply@astron.nl'
from dataclasses import dataclass

@dataclass
class AttachmentFile:
    fname: str
    content: str
    mime: str = 'text/plain'


def generate_from_template_str(template: str, context: Context):
    template = Template(template)
    return template.render(context)


def send_email_from_template(template: str, context: Context,
                             to_address,
                             subject,
                             attachments=None):
    if attachments is None:
        attachments = []
    message = generate_from_template_str(template, context)
    email = EmailMessage(subject, message, FROM_EMAIL, to=[to_address])
    for attachment in attachments:
        email.attach(filename=attachment.fname,
                     content=attachment.content,
                     mimetype=attachment.mime)
    email.send()
