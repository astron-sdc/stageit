from django.core.management.base import BaseCommand

from stage_api.models.lta import LTARequest


class Command(BaseCommand):
    help = "Create LTA Site and LTA Request relations"

    def handle(self, *args, **options):
        requests = LTARequest.objects.all()
        for request in requests:
            request.set_lta_sites_from_surls()
