from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from stage_api.models.lta import LTARequest
from stage_api.models.draft import DraftLTARequest

class StageApiTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.admin_user = User.objects.create_superuser(username='admin', password='adminpassword')
        self.token, _ = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.lta_request = LTARequest.objects.create(user=self.user)
        self.draft_lta_request = DraftLTARequest.objects.create(surls=['test'])

    def test_convert_surls_to_webdav(self):
        response = self.client.get(reverse('get-webdav-urls', args=[self.lta_request.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_convert_surls_to_gridftp(self):
        response = self.client.get(reverse('get-gridftp-urls', args=[self.lta_request.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_stageit_metrics_view(self):
        response = self.client.get(reverse('get-stageit-metrics'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_stageit_get_lta_site_combined_info_view(self):
        response = self.client.get(reverse('get-stageit-ltasite-info'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
