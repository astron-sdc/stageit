from django.test import TestCase
from django.contrib.auth.models import User
from stage_api.models.lta import LTARequest, AffectedSurls, LTASite, LTAMacaroon, link_surls_to_lta_request, LTARequestType

from stage_api.utils import *

SURLS = [
    "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B016_P000_bf_2c07fc7b.tar",
    "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B019_P000_bf_3a5b0299.tar",
    "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B021_P000_bf_ea869dc6.tar"]

EXPECTED_DOWNLOAD_URLS = [
    "https://lofar-download.grid.surfsara.nl/lofigrid/SRMFifoGet.py?surl=srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B016_P000_bf_2c07fc7b.tar",
    "https://lofar-download.grid.surfsara.nl/lofigrid/SRMFifoGet.py?surl=srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B019_P000_bf_3a5b0299.tar",
    "https://lofar-download.grid.surfsara.nl/lofigrid/SRMFifoGet.py?surl=srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B021_P000_bf_ea869dc6.tar"]

EXPECTED_GRIDFTP_URLS = [
    "gsiftp://gridftp.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B016_P000_bf_2c07fc7b.tar",
    "gsiftp://gridftp.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B019_P000_bf_3a5b0299.tar",
    "gsiftp://gridftp.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/455576/L455576_SAP002_B021_P000_bf_ea869dc6.tar"]

EXPECTED_WEBDAV_URLS = [
    "https://webdav.grid.surfsara.nl:2882/lt5_004/455576/L455576_SAP002_B016_P000_bf_2c07fc7b.tar",
    "https://webdav.grid.surfsara.nl:2882/lt5_004/455576/L455576_SAP002_B019_P000_bf_3a5b0299.tar",
    "https://webdav.grid.surfsara.nl:2882/lt5_004/455576/L455576_SAP002_B021_P000_bf_ea869dc6.tar"]


def _set_urls(request, surls, size_per_url):
    for url in surls:
        aff_surl = AffectedSurls(surl=url, request=request, size=size_per_url)
        aff_surl.save()
    return request, surls

def create_request_with_macaroon_and_lta_site_for_testing(skip_macaroon=False):
    lta_request = LTARequest()
    lta_request.save()

    a_surls = [AffectedSurls(surl=surl) for surl in SURLS]
    link_surls_to_lta_request(a_surls, lta_request)

    lta_site = LTASite(name='test_surf_site', surl_pattern='srm://srm.grid.sara.nl:8443',
                       download_server_url="https://lofar-download.grid.surfsara.nl/lofigrid/SRMFifoGet.py?surl=srm://srm.grid.sara.nl:8443",
                       gridftp_base_url="gsiftp://gridftp.grid.sara.nl",
                       webdav_base_url="https://webdav.grid.surfsara.nl:2882")
    lta_site.save()

    if not skip_macaroon:
        lta_macaroon = LTAMacaroon(requested_by=lta_request, content="ILikeCookies", lta_site=lta_site,
                                   caveat_root_path="/pnfs/grid.sara.nl/data/lofar/ops/projects")
        lta_macaroon.save()
    return lta_request


class TestConversionFunctions(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestConversionFunctions, cls).setUpClass()
        cls.created_request = create_request_with_macaroon_and_lta_site_for_testing()

    def test_srm_to_download(self):
        for surl, expected_result in zip(SURLS, EXPECTED_DOWNLOAD_URLS):
            result = srm_to_download(surl)
            self.assertEqual(result, expected_result, f"Mismatch in converting {surl}")

    def test_srm_to_gridftp(self):
        for surl, expected_result in zip(SURLS, EXPECTED_GRIDFTP_URLS):
            result = srm_to_gridftp(surl)
            self.assertEqual(result, expected_result, f"Mismatch in converting {surl}")

    def test_srm_to_webdav(self):
        for surl, expected_result in zip(SURLS, EXPECTED_WEBDAV_URLS):
            result = srm_to_webdav(surl, self.created_request)
            self.assertEqual(result, expected_result, f"Mismatch in converting {surl}")

    def test_get_retried_request_ids_per_lta_site(self):
        """test get_retried_request_ids_per_lta_site when there are no retries defined in the request
           the expected result is an empty dictionary"""
        expected_result = {}
        result = get_retried_request_ids_per_lta_site()
        self.assertEqual(result, expected_result)

    def test_get_total_surl_size_per_retry_count_per_lta_site(self):
        """test get_total_surl_size_per_retry_count_per_lta_site when there are no retries defined in the request
           the expected result is an empty dictionary"""
        expected_result = {}
        result = get_total_surl_size_per_retry_count_per_lta_site()
        self.assertEqual(result, expected_result)


class TestConversionFunctionsNoMacaroon(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestConversionFunctionsNoMacaroon, cls).setUpClass()
        cls.created_request = create_request_with_macaroon_and_lta_site_for_testing(skip_macaroon=True)

    def test_srm_to_webdav_no_macaroon(self):
        """
        If no macaroon object is created, no webdav urls are available
        """
        expected_result = ""
        for surl in SURLS:
            result = srm_to_webdav(surl, self.created_request)
            self.assertEqual(result, expected_result, f"Mismatch in converting {surl}")


class TestAggregationFunctions(TestCase):
    """
    Perform testcases on 'aggregated util' functions with setup with test data in datamodel (LTA request, LTASite, AffectedUrls)
    """

    def setUp(self):
        """ Set up test data:
        - Request 1 (Site A, retry_count: 2, total_surls: 5, total_size: 500, User X)
        - Request 2 (Site A, retry_count: 1, total_surls: 3, total_size: 300, User X)
        - Request 3 (Site B, retry_count: 2, total_surls: 4, total_size: 400, User X)
        - Request 4 (Site B, retry_count: 1, total_surls: 2, total_size: 200, User X)
        - Request 5 (Site B, retry_count: 2, total_surls: 1, total_size: 100, User Y)
        """
        self.user_x = User(username="testerX")
        self.user_x.save()
        self.user_y = User(username="testerY")
        self.user_y.save()

        self.site_a = LTASite(name="Site A", surl_pattern=r"site_a_pattern")
        self.site_a.save()
        self.site_b = LTASite(name="Site B", surl_pattern=r"site_b_pattern")
        self.site_b.save()

        self.request_1 = LTARequest(request_type=LTARequestType.STAGE, retry_count=2, user=self.user_x)
        self.request_2 = LTARequest(request_type=LTARequestType.STAGE, retry_count=1, user=self.user_x)
        self.request_3 = LTARequest(request_type=LTARequestType.STAGE, retry_count=2, user=self.user_x)
        self.request_4 = LTARequest(request_type=LTARequestType.STAGE, retry_count=1, user=self.user_x)
        self.request_5 = LTARequest(request_type=LTARequestType.STAGE, retry_count=2, user=self.user_y)
        self.request_1.save()
        self.request_2.save()
        self.request_3.save()
        self.request_4.save()
        self.request_5.save()
        self.request_1.lta_sites.set([self.site_a])
        self.request_2.lta_sites.set([self.site_a])
        self.request_3.lta_sites.set([self.site_b])
        self.request_4.lta_sites.set([self.site_b])
        self.request_5.lta_sites.set([self.site_b])

        # Set urls and size per surl
        _set_urls(self.request_1, ["surl1","surl2","surl3","surl4","surl5"], 100)
        _set_urls(self.request_2, ["surl1","surl2","surl3"], 100)
        _set_urls(self.request_3, ["surl1","surl2","surl3","surl4"], 100)
        _set_urls(self.request_4, ["surl1","surl2"], 100)
        _set_urls(self.request_5, ["surl1"], 100)

    def test_get_total_surl_size_per_retry_count_per_lta_site(self):
        expected_result = {"Site A": {
                                     1: {"total_surls": 3,
                                         "total_size": 300,
                                         "total_request_ids": 1},
                                     2: {"total_surls": 5,
                                         "total_size": 500,
                                         "total_request_ids": 1}},
                           "Site B": {
                                     1: {"total_surls": 2,
                                         "total_size": 200,
                                         "total_request_ids": 1},
                                     2: {"total_surls": 5,
                                         "total_size": 500,
                                         "total_request_ids": 2}}}
        result = get_total_surl_size_per_retry_count_per_lta_site()
        self.assertEqual(result, expected_result)

    def test_get_total_surl_size_and_retry_count_per_lta_site(self):
        expected_result = {'Site A': {'total_surls': 8, 'total_size': 800, 'total_retries': 3, 'total_request_ids': 2},
                           'Site B': {'total_surls': 7, 'total_size': 700, 'total_retries': 5, 'total_request_ids': 3}}
        result = get_total_surl_size_and_retry_count_per_lta_site()
        self.assertEqual(result, expected_result)


    def test_get_retried_request_ids_per_lta_site(self):
        expected_result = {"Site A": {2: [self.request_1.id],
                                      1: [self.request_2.id]},
                           "Site B": {2: [self.request_3.id, self.request_5.id],
                                      1: [self.request_4.id]}}
        result = get_retried_request_ids_per_lta_site()
        self.assertEqual(result, expected_result)


    def test_get_total_surl_size_and_retry_count_per_user(self):
        expected_result = {self.user_x.id:
                               {'total_surls': 14, 'total_size': 1400, 'total_retries': 23, 'total_request_ids': 4},
                           self.user_y.id:
                               {'total_surls': 1, 'total_size': 100, 'total_retries': 2, 'total_request_ids': 1}}

        result = get_total_surl_size_and_retry_count_per_user()
        self.assertEqual(result, expected_result)
