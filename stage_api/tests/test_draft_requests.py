from unittest.mock import MagicMock

from django.contrib.auth.models import User
from django.test import TestCase

from stage_api.models.draft import DraftLTARequest
from stage_api.models.lta import AffectedSurls, LTARequest, link_surls_to_lta_request
from stage_api.serializers import DraftLTARequestSerializer

TEST_SURLS = ["srm://somewhereovertherainbow/file1.tar", "srm://somewhereovertherainbow/pic1.tar",
              "srm://somewhereovertherainbow/mybesttrip.tar", "gsiftp://somewhereovertherainbow/mybesttrip.tar"]
TEST_INVALID_SURLS = ["https://somewhereundertheearth/fil1.tar", "https://somewherearoundmars/fil1.tar",
                      "file://whereeveryoucanfindpeace/fslt.tar", "nowhere/somewhere/everywhere.tar",
                      "as90dhbi12qewdhcs89ubfai3wirjifsd23"]


class TestDraftRequests(TestCase):

    def test_surl_linking_ok(self):
        a_surls = [AffectedSurls(surl=surl) for surl in TEST_SURLS]
        for asurl in a_surls:
            asurl.update_project_info = MagicMock()

        request = LTARequest(request_type="stage", )
        request.save()

        final = link_surls_to_lta_request(a_surls, request)

        self.assertCountEqual(final.surls.all(), a_surls)
        linked_surls = []
        for surl in a_surls:
            surl.update_project_info.assert_called_once()
            linked_surls.append(surl.surl)

        self.assertEqual(sorted(linked_surls), sorted(TEST_SURLS))

    def test_surl_linking_failed_request_not_saved(self):
        a_surls = [AffectedSurls(surl=surl) for surl in TEST_SURLS]
        for asurl in a_surls:
            asurl.update_project_info = MagicMock()

        request = LTARequest(request_type="stage", )

        with self.assertRaises(ValueError):
            link_surls_to_lta_request(a_surls, request)

    def test_validate_surls_valid(self):
        serializer = DraftLTARequestSerializer(data={"surls": TEST_SURLS, "request_type": "stage"})

        self.assertEqual(serializer.is_valid(), True)

    def test_validate_surls_invalid(self):
        serializer = DraftLTARequestSerializer(data={"surls": TEST_INVALID_SURLS, "request_type": "stage"})

        self.assertEqual(serializer.is_valid(), False)
        self.assertIn("surls", serializer.errors)

    def test_validate_surls_mixed(self):
        serializer = DraftLTARequestSerializer(data={"surls": TEST_INVALID_SURLS, "request_type": "stage"})

        self.assertEqual(serializer.is_valid(), False)
        self.assertIn("surls", serializer.errors)

    def test_notify_default_true(self):
        draft = DraftLTARequest(surls=['test'])
        request = draft.create_request(user=None)
        self.assertTrue(request.notify)

    def test_request_type_is_set(self):
        draft = DraftLTARequest(surls=['test'], request_type='destroy_planet')
        request = draft.create_request(user=None)
        self.assertEqual(request.request_type, 'destroy_planet')

    def test_user_is_set(self):
        user = User(id=1)
        user.save()
        draft = DraftLTARequest(surls=['test'])
        request = draft.create_request(user=1)
        self.assertEqual(request.user, user)
