import unittest.mock as mock

from django.core import mail
from django.test import TestCase
from rest_framework.authtoken.admin import User

from stage_api.emails import send_email_from_template, generate_from_template_str, Context
from stage_api.models.email import EmailTemplate, EmailTemplateFile
from stage_api.models.lta import LTARequest, AffectedSurls, LTARequestStatus, LTASite, LTAMacaroon
from stage_api.tasks import handle_status_change


def create_request_with_email_template_and_lta_site_for_testing(template):
    srm_example = 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_011/149478' \
                  '/L149478_SAP000_B000_S3_P015_bf.h5.tar'

    request = LTARequest()
    request.save()

    asurl = AffectedSurls(surl=srm_example, request=request)
    asurl.save()

    et = EmailTemplate(template='', subject='test', transition_from_status='S', transition_to_status='R')
    et.save()

    etf = EmailTemplateFile(template=template, file_name='my_nice_fname', email_template=et)
    etf.save()

    lta_site = LTASite(name='test_site_somewhere', surl_pattern='srm://srm.grid.sara.nl:8443',
                       download_server_url="https://nice_download_site/srm=",
                       gridftp_base_url="gsiftp://gridftp.my_grid.sara.nl",
                       webdav_base_url="https://my_webdav")
    lta_site.save()
    return request, etf, lta_site


class TestEmailTemplate(TestCase):
    def setUp(self) -> None:
        self.TEMPLATE = 'HERE I AM TESTING THE {{name}}'
        self.EMAIL = 'user_email@user.nl'
        self.SUBJECT = 'subject'
        self.CONTEXT = Context({'name': 'TEMPLATE FUNCTIONALITY'})
        self.EXPECTED_CONTENT = self.TEMPLATE.replace('{{name}}', self.CONTEXT['name'])

        self.email_template = EmailTemplate(subject='test template', template='{{request.type}}',
                                            transition_from_status='P', transition_to_status='R', request_type='stage')

        self.lta_request_with_email = LTARequest(request_type='stage', email="mockito@mocky.mock", notify=False)
        self.lta_request_no_email = LTARequest(request_type='stage', notify=False, user=User(email="user@foo.bar"))

    def test_template_generation(self):
        string = generate_from_template_str(self.TEMPLATE, Context({'name': 'TEMPLATE FUNCTIONALITY'}))
        self.assertEqual(self.EXPECTED_CONTENT, string)

    def test_email_sending_no_attachment(self):
        send_email_from_template(self.TEMPLATE, self.CONTEXT, self.EMAIL, self.SUBJECT)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, self.SUBJECT)
        self.assertEqual(mail.outbox[0].body, self.EXPECTED_CONTENT)
        self.assertEqual(mail.outbox[0].to, [self.EMAIL])

    @mock.patch("stage_api.models.email.send_email_from_template")
    @mock.patch("stage_api.models.email.EmailTemplate.attachments")
    def test_send_address_from_request(self, email_template_attachments_mock: mock.MagicMock,
                                       send_email_from_template_mock: mock.MagicMock):
        email_template_attachments_mock.all.return_value = []
        EmailTemplate().send(self.lta_request_with_email)
        args, kwargs = send_email_from_template_mock.call_args
        self.assertEqual(self.lta_request_with_email.email, kwargs.get("to_address"))

    @mock.patch("stage_api.models.email.send_email_from_template")
    @mock.patch("stage_api.models.email.EmailTemplate.attachments")
    def test_send_address_from_user(self, email_template_attachments_mock: mock.MagicMock,
                                    send_email_from_template_mock: mock.MagicMock):
        email_template_attachments_mock.all.return_value = []
        EmailTemplate().send(self.lta_request_no_email)
        args, kwargs = send_email_from_template_mock.call_args
        self.assertEqual(self.lta_request_no_email.user.email, kwargs.get("to_address"))

    def test_generate_attachments(self):
        srm_example = 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_011/149478' \
                      '/L149478_SAP000_B000_S3_P015_bf.h5.tar'

        template = '''
{% load surl_transform %}
{% for srm in lta_request.surls.all %}
    {{ srm.surl|srm_to_download }}
{% endfor %}
        '''
        request = LTARequest()
        request.save()

        asurl = AffectedSurls(surl=srm_example, request=request)
        asurl.save()

        et = EmailTemplate(template='', subject='test', transition_from_status='S', transition_to_status='R')
        et.save()

        etf = EmailTemplateFile(template=template, file_name='my_nice_fname', email_template=et)
        etf.save()

        lta_site = LTASite(name='test_site', surl_pattern='(srm://srm\\.grid\\.sara\\.nl:8443/.*)',
                           download_server_url='https://nice_download_site/srm=\\1')
        lta_site.save()
        result = etf.generate_attachment(request)

        self.assertEqual(result.content.strip(), f'https://nice_download_site/srm={srm_example}')

    def test_generate_attachments_gridftp(self):
        srm_example = 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_011/149478' \
                      '/L149478_SAP000_B000_S3_P015_bf.h5.tar'

        template = '''
{% load surl_transform %}
{% for srm in lta_request.surls.all %}
    {{ srm.surl|srm_to_gridftp }}
{% endfor %}
        '''
        request, etf, lta_site = create_request_with_email_template_and_lta_site_for_testing(template)
        result = etf.generate_attachment(request)

        self.assertEqual(result.content.strip(), "gsiftp://gridftp.my_grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/projects/lc0_011/149478/L149478_SAP000_B000_S3_P015_bf.h5.tar")

#    grid_ftp_path = "gsiftp://gridftp.my_grid.sara.nl'",

    def test_generate_attachments_webdav(self):
        template = '''
{% load surl_transform %}
{% for surl in lta_request.surls.all %}
    {% srm_to_webdav surl.surl lta_request %}
{% endfor %}
        '''
        request, etf, lta_site = create_request_with_email_template_and_lta_site_for_testing(template)
        lta_macaroon = LTAMacaroon(requested_by=request, content="ILikeCookies", lta_site=lta_site,
                                   caveat_root_path="/pnfs/grid.sara.nl/data/lofar/ops/projects")
        lta_macaroon.save()

        result = etf.generate_attachment(request)
        self.assertEqual(result.content.strip(), f'https://my_webdav/lc0_011/149478/L149478_SAP000_B000_S3_P015_bf.h5.tar')

    def test_generate_attachments_webdav_when_no_macaroon_created(self):
        """When macaroon is not created for a request attachment is empty"""
        template = '''
{% load surl_transform %}
{% for surl in lta_request.surls.all %}
    {% srm_to_webdav surl.surl lta_request %}
{% endfor %}
        '''
        request, etf, lta_site = create_request_with_email_template_and_lta_site_for_testing(template)

        result = etf.generate_attachment(request)
        self.assertEqual(result.content.strip(), "")

    def test_generate_attachments_macaroon(self):
        template = '''
{% for mac in lta_request.macaroons.all %}
The macaroon for LTA Site {{mac.lta_site.name}} is:
{{mac.content}}
{% endfor %}
        '''
        request, etf, lta_site = create_request_with_email_template_and_lta_site_for_testing(template)

        lta_macaroon = LTAMacaroon(requested_by=request, content="ILikeCookies", lta_site=lta_site,
                                   caveat_root_path="/pnfs/grid.sara.nl/data/lofar/ops/projects")
        lta_macaroon.save()

        result = etf.generate_attachment(request)
        self.assertEqual(result.content.strip(), f'The macaroon for LTA Site test_site_somewhere is:\nILikeCookies')

    def test_generate_attachments_macaroon_when_no_macaroon_created(self):
        """When macaroon is not created for a request attachment is empty"""
        template = '''
    {% for mac in lta_request.macaroons.all %}
    The macaroon for LTA Site {{mac.lta_site.name}} is:
    {{mac.content}}
    {% endfor %}
        '''
        request, etf, lta_site = create_request_with_email_template_and_lta_site_for_testing(template)

        result = etf.generate_attachment(request)
        self.assertEqual(result.content.strip(), "")


class TestEmailSendOnTransition(TestCase):
    def setUp(self):
        self.lta_request_notify_false = LTARequest(request_type='stage', email='mrmock@mockery.mock', notify=False)
        self.lta_request_notify_true = LTARequest(request_type='stage', email='test@testemail.nl', notify=True)
        self.lta_request_notify_true.save()
        self.surl = AffectedSurls(surl='srm://test_surl', request=self.lta_request_notify_true)
        self.surl.save()

        self.email_template = EmailTemplate(subject='test', template='{{request.type}}', transition_from_status='S',
                                            transition_to_status='P', request_type='stage')
        self.email_template.save()

    @mock.patch("stage_api.models.lta.LTARequest.objects")
    @mock.patch("stage_api.models.email.EmailTemplate.objects")
    @mock.patch.object(EmailTemplate, "send")
    def test_notify_false(self, mock_email_template_send: mock.MagicMock, mock_email_template_objects,
                          mock_ltarequest_objects):
        mock_ltarequest_objects.get.return_value = self.lta_request_notify_false
        mock_email_template_objects.get.return_value = self.email_template
        handle_status_change(5, "stage", "P", "S")
        mock_email_template_send.assert_not_called()

    @mock.patch("stage_api.models.lta.LTARequest.objects")
    @mock.patch("stage_api.models.email.EmailTemplate.objects")
    @mock.patch.object(EmailTemplate, "send")
    def test_notify_true(self, mock_email_template_send: mock.MagicMock, mock_email_template_objects,
                         mock_ltarequest_objects):
        mock_ltarequest_objects.get.return_value = self.lta_request_notify_true
        mock_email_template_objects.get.return_value = self.email_template
        handle_status_change(5, "stage", "P", "S")
        mock_email_template_send.assert_called_once()

    def test_transition(self):
        with mock.patch('stage_api.models.lta.celery_app.send_task') as mock_send_task:
            lta_request_status = LTARequestStatus(status_code='P', request=self.lta_request_notify_true)
            lta_request_status.save()
            mock_send_task.assert_called_with('stage_api.tasks.handle_status_change',
                                              args=[self.lta_request_notify_true.pk, 'stage', 'S', 'P'])
            handle_status_change(self.lta_request_notify_true.pk, 'stage', 'S', 'P')

            self.assertEqual(1, len(mail.outbox))
            self.assertEqual(mail.outbox[0].subject, self.email_template.subject)

    def test_task_called(self):
        with mock.patch('stage_api.models.lta.celery_app.send_task') as mock_send_task:
            lta_request_status = LTARequestStatus(status_code='P', request=self.lta_request_notify_true)
            lta_request_status.save()

            lta_request = LTARequest.objects.get(pk=lta_request_status.request.pk)
            self.assertEqual(lta_request.current_status_code, 'P')
            mock_send_task.assert_called_with('stage_api.tasks.handle_status_change',
                                              args=[self.lta_request_notify_true.pk, 'stage', 'S', 'P'])
