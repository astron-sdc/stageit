from django.test import TestCase

from stage_api.models.lta import AffectedSurls


class TestProjectRegex(TestCase):

    def test_project_parsing(self):
        surls = ['srm://my_lta_site/my_favourite_path/projects/LT1011/data/file.tar',
                 'srm://my_lta_site/my_favourite_path/projects/LT1_11/data/file.tar',
                 'srm://my_lta_site/my_favourite_path/projects/111111/data/file.tar',
                 'srm://my_lta_site/my_favourite_path/projects/dummy/data/file.tar',
                 'srm://my_lta_site/my_favourite_path/projects/lt10_10/data/file.tar',
                 '',
                 'srm://my_lta_site/my_favourite_path/none/lt10_10/data/file.tar',
                 'srm://my_lta_site/my_favourite_path/projects/lt10_10/projects/lt11_10/file.tar',
                 ]
        expected_result = ['LT1011', 'LT1_11', '111111', 'dummy', 'lt10_10', 'undefined', 'undefined', 'lt11_10']

        for surl, expected in zip(surls, expected_result):
            af = AffectedSurls(surl=surl)
            self.assertEqual(af.project, expected)
