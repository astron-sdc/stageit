from allauth.socialaccount.models import SocialAccount, SocialLogin
from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory

from stage_api.adapter import SocialAccountAdapter
from stage_api.models.user_info import ProjectRole, UserInfo


class TestSocialAdapter(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.req = self.factory.request()
        self.req.session = self.client.session

    def test_user_info_stored(self):
        adapter = SocialAccountAdapter()

        sociallogin = SocialLogin(user=User(username="gealle", password="secret"), account=SocialAccount(
            extra_data={"email_verified": False, "name": "Piebe Gealle", "preferred_username": "gealle",
                "given_name": "Piebe", "family_name": "Gealle", "email": "test@example.com",
                "sub": "ff573e03-423b-4819-a174-6bd7438ebd14", "l": "The Netherlands",
                "eduperson_entitlement": ["urn:mace:astron.nl:science:group:lofar:affiliation:affiliation_705",
                    "urn:mace:astron.nl:science:group:lofar:role=lta_support",
                    "urn:mace:astron.nl:science:group:lofar:role=lta_user",
                    "urn:mace:astron.nl:science:group:lofar:project:rolloutjune17",
                    "urn:mace:astron.nl:science:group:lofar:project:rolloutjune17:role=friend_of_project", ], }), )

        adapter.save_user(self.req, sociallogin)

        users = User.objects.all()
        self.assertEqual(len(users), 1, "Only 1 user should be created")

        # Verify that the necessary objects are created
        user = User.objects.get(username="gealle")
        self.assertTrue(user)

        profile = UserInfo.objects.get(user=user)
        self.assertEqual(profile.location, "The Netherlands")

        roles = ProjectRole.objects.filter(user_info=profile).all()
        self.assertEqual(roles[0].project, "rolloutjune17")
        self.assertListEqual(roles[0].roles, ["friend_of_project"])

    def test_only_location_provided(self):
        adapter = SocialAccountAdapter()

        sociallogin = SocialLogin(user=User(username="gealle", password="secret"), account=SocialAccount(
            extra_data={"email_verified": False, "name": "Piebe Gealle", "preferred_username": "gealle",
                "given_name": "Piebe", "family_name": "Gealle", "email": "test.example.com",
                "sub": "ff573e03-423b-4819-a174-6bd7438ebd14", "l": "The Netherlands", }), )

        adapter.save_user(self.req, sociallogin)

        # Verify that the necessary objects are created
        user = User.objects.get(username="gealle")
        self.assertTrue(user)

        profile = UserInfo.objects.get(user=user)
        self.assertEqual(profile.location, "The Netherlands")

        roles = ProjectRole.objects.filter(user_info=profile).all()
        self.assertEqual(len(roles), 0)

    def test_no_info_provided(self):
        adapter = SocialAccountAdapter()

        sociallogin = SocialLogin(user=User(username="gealle", password="secret"), account=SocialAccount(
            extra_data={"email_verified": False, "name": "Piebe Gealle", "preferred_username": "gealle",
                "given_name": "Piebe", "family_name": "Gealle", "email": "test.example.com",
                "sub": "ff573e03-423b-4819-a174-6bd7438ebd14", }), )

        adapter.save_user(self.req, sociallogin)

        # Verify that the necessary objects are created
        user = User.objects.get(username="gealle")
        self.assertTrue(user)

        profile = UserInfo.objects.get(user=user)
        self.assertIsNone(profile.location)

        roles = ProjectRole.objects.filter(user_info=profile).all()
        self.assertEqual(len(roles), 0)

    def test_only_project_info_supplied(self):
        adapter = SocialAccountAdapter()

        sociallogin = SocialLogin(user=User(username="gealle", password="secret"), account=SocialAccount(
            extra_data={"email_verified": False, "name": "Piebe Gealle", "preferred_username": "gealle",
                "given_name": "Piebe", "family_name": "Gealle", "email": "test.example.com",
                "sub": "ff573e03-423b-4819-a174-6bd7438ebd14",
                "eduperson_entitlement": ["urn:mace:astron.nl:science:group:lofar:affiliation:affiliation_705",
                    "urn:mace:astron.nl:science:group:lofar:role=lta_support",
                    "urn:mace:astron.nl:science:group:lofar:role=lta_user",
                    "urn:mace:astron.nl:science:group:lofar:project:rolloutjune17",
                    "urn:mace:astron.nl:science:group:lofar:project:rolloutjune17:role=friend_of_project", ], }), )

        adapter.save_user(self.req, sociallogin)

        # Verify that the necessary objects are created
        user = User.objects.get(username="gealle")
        self.assertTrue(user)

        profile = UserInfo.objects.get(user=user)
        self.assertIsNone(profile.location)

        roles = ProjectRole.objects.filter(user_info=profile).all()
        self.assertEqual(roles[0].project, "rolloutjune17")
        self.assertListEqual(roles[0].roles, ["friend_of_project"])

    def test_only_project_info_supplied_supports_project_without_roles(self):
        adapter = SocialAccountAdapter()

        sociallogin = SocialLogin(user=User(username="gealle", password="secret"), account=SocialAccount(
            extra_data={"email_verified": False, "name": "Piebe Gealle", "preferred_username": "gealle",
                "given_name": "Piebe", "family_name": "Gealle", "email": "test.example.com",
                "sub": "ff573e03-423b-4819-a174-6bd7438ebd14",
                "eduperson_entitlement": ["urn:mace:astron.nl:science:group:lofar:affiliation:affiliation_705",
                    "urn:mace:astron.nl:science:group:lofar:role=lta_support",
                    "urn:mace:astron.nl:science:group:lofar:role=lta_user",
                    "urn:mace:astron.nl:science:group:lofar:project:rolloutjune17",
                    "urn:mace:astron.nl:science:group:lofar:project:rolloutjune17:role=friend_of_project",
                    'urn:mace:astron.nl:science:group:lofar:project:lc13_023']}), )

        adapter.save_user(self.req, sociallogin)

        # Verify that the necessary objects are created
        user = User.objects.get(username="gealle")
        self.assertTrue(user)

        profile = UserInfo.objects.get(user=user)
        self.assertIsNone(profile.location)

        roles = ProjectRole.objects.filter(user_info=profile, project='rolloutjune17').all()
        self.assertEqual(roles[0].project, "rolloutjune17")
        self.assertListEqual(roles[0].roles, ["friend_of_project"])

        roles = ProjectRole.objects.filter(user_info=profile, project='lc13_023').all()
        self.assertEqual(roles[0].project, "lc13_023")
        self.assertListEqual(roles[0].roles, [])
