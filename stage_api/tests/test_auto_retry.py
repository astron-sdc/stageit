from unittest.mock import patch

from django.conf import settings
from django.core import mail
from django.test import TestCase, override_settings

from stage_api import tasks
from stage_api.models.lta import LTARequest, LTARequestStatusCode, AffectedSurls, LTARequestType

SURLS = ['srm://my_lta_site/my_favourite_path/projects/LT1011/data/file.tar',
         'srm://my_lta_site/my_favourite_path/projects/LT1_11/data/file.tar',
         'srm://my_lta_site/my_favourite_path/projects/111111/data/file.tar',
         'srm://my_lta_site/my_favourite_path/projects/dummy/data/file.tar',
         'srm://my_lta_site/my_favourite_path/projects/lt10_10/data/file.tar']


def _create_request(notify=False, retry_count=0):
    request = LTARequest.objects.create(request_type=LTARequestType.STAGE, notify=notify, retry_count=retry_count,
                                        email="no-reply@example.com")
    for url in SURLS:
        aff_surl = AffectedSurls(surl=url, request=request)
        aff_surl.save()

    return request


@override_settings(CELERY_TASK_ALWAYS_EAGER=True)
class TestAutoRetry(TestCase):

    def test_retry_in_place_request(self):
        request = _create_request()
        request.retry_in_place()
        new_request = LTARequest.objects.all()
        self.assertEqual(len(new_request), 1, "should only be one request")

        # Retry cound should be incremented
        self.assertEqual(new_request[0].retry_count, 1)

        # Attributes should match
        self.assertEqual(new_request[0].request_type, request.request_type)
        self.assertEqual(new_request[0].user, request.user)
        self.assertEqual(new_request[0].notify, request.notify)
        self.assertEqual(new_request[0].email, request.email)

    @patch("stage_api.models.lta.celery_app.send_task")  # send task does not play nice with eager execution
    def test_on_failure_on_first_invocation(self, _):
        request = _create_request()
        t = tasks.TaskWithErrorHandling()
        t.on_failure(RuntimeError("Kaboom!"), "task_id", [request.pk], {}, None)
        request.refresh_from_db()
        self.assertEqual(request.current_status_code, LTARequestStatusCode.SUBMITTED,
                         "Original request should be in submitted again")

    @patch("stage_api.models.lta.celery_app.send_task")  # send task does not play nice with eager execution
    def test_on_failure_on_third_invocation(self, _):
        request = _create_request(retry_count=2)
        t = tasks.TaskWithErrorHandling()
        t.on_failure(RuntimeError("Kaboom!"), "task_id", [request.pk], {}, None)
        request.refresh_from_db()
        self.assertEqual(request.current_status_code, LTARequestStatusCode.ERROR, "Original request should be in error")

    def test_handle_status_change_error_first_invocation(self):
        request = _create_request(notify=True)
        tasks.handle_status_change(request.pk, request.request_type, LTARequestStatusCode.RUNNING,
                                   LTARequestStatusCode.ERROR)
        self.assertEqual(len(mail.outbox), 0, "There should be no mail in outbox")

    def test_handle_status_change_happy_followup_invocations(self):
        request = _create_request(notify=True, retry_count=1)
        tasks.handle_status_change(request.pk, request.request_type, LTARequestStatusCode.PENDING,
                                   LTARequestStatusCode.RUNNING)
        self.assertEqual(len(mail.outbox), 0, "There should be no mail in outbox")

    def test_handle_status_change_error_third_invocation(self):
        request = _create_request(notify=True, retry_count=2)
        tasks.handle_status_change(request.pk, request.request_type, LTARequestStatusCode.RUNNING,
                                   LTARequestStatusCode.ERROR)
        self.assertEqual(len(mail.outbox), 1, "There should be a single mail in outbox")
