from django.test import TestCase

from stage_api.adapter import parse_entitlements_into_project_roles

TEST_DATA = \
    ['urn:mace:astron.nl:science:group:lofar:affiliation:affiliation_705',
     'urn:mace:astron.nl:science:group:lofar:project:rolloutjune17',
     'urn:mace:astron.nl:science:group:lofar:project:rolloutjune17:role=friend_of_project',
     'urn:mace:astron.nl:science:group:lofar:project:rolloutjune17:role=enemy_of_project',
     'urn:mace:astron.nl:science:group:lofar:role=lta_support',
     'urn:mace:astron.nl:science:group:lofar:role=lta_user']

TEST_DATA_NO_PROJECT = [
    'urn:mace:astron.nl:science:group:lofar:affiliation:affiliation_705',
    'urn:mace:astron.nl:science:group:lofar:role=lta_support',
    'urn:mace:astron.nl:science:group:lofar:role=lta_user']

EXPECTED_RESULT = ['rolloutjune17']
EXPECTED_ROLES = ['friend_of_project', 'enemy_of_project']


class TestProjectEntitelment(TestCase):

    def test_parsing_project(self):
        results = parse_entitlements_into_project_roles(TEST_DATA)

        self.assertEqual(EXPECTED_RESULT, [str(result.project) for result in results])
        self.assertEqual(set(EXPECTED_ROLES), set(results[0].roles))

    def test_parsing_no_entitlement(self):
        results = parse_entitlements_into_project_roles([])
        self.assertEqual([], [str(result.project) for result in results])

    def test_parsing_none_entitlement(self):
        results = parse_entitlements_into_project_roles(None)
        self.assertEqual([], [str(result.project) for result in results])

    def test_parsing_no_project(self):
        results = parse_entitlements_into_project_roles(TEST_DATA_NO_PROJECT)
        self.assertEqual([], [str(result.project) for result in results])
