from unittest import mock

from django.test import TestCase

from stage_api.models.lta import LTARequest, LTARequestStatus


class TestStatusChange(TestCase):
    def setUp(self) -> None:
        self.request = LTARequest(current_status_code='S', request_type='stage')
        self.request.save()

        self.request_status = LTARequestStatus(request=self.request, status_code='S')
        self.request_status.save()

    @mock.patch("stage_api.models.lta.celery_app.send_task")
    def test_send_task(self, celery_send_task_mock: mock.MagicMock):
        self.request.current_status_code = 'P'
        self.request.save(update_fields=['current_status_code'])
        celery_send_task_mock.assert_called_once_with('stage_api.tasks.handle_status_change',
                                                      args=[self.request.id, 'stage', 'S', 'P'])

    @mock.patch("stage_api.models.lta.celery_app.send_task")
    def test_do_not_send_task(self, celery_send_task_mock: mock.MagicMock):
        self.request.request_token = '123'
        self.request.save(update_fields=['request_token'])
        celery_send_task_mock.assert_not_called()

    @mock.patch("stage_api.models.lta.LTARequestStatus.request")
    def test_request_status_saves_request(self, request_mock: mock.MagicMock):
        self.request_status.status_code = 'P'
        self.request_status.save()
        request_mock.save.assert_called_once_with(update_fields=['current_status_code'])
