from unittest.mock import patch

from django.test import TestCase
from django.utils import timezone

from stage_api.models.lta import LTARequest, LTARequestStatus, LTAError, LTAMacaroon, LTASite


class TestLTARequestStatuses(TestCase):

    @patch('stageit.celery.app.send_task')
    def test_last_status_change(self, _):
        request = LTARequest(request_type='stage')
        request.save()

        self.assertEqual(request.last_status_change, request.when)
        request_status_pending = LTARequestStatus(status="P", request=request)
        request_status_pending.save()
        self.assertEqual(request.last_status_change, request_status_pending.when)
        request_status_running = LTARequestStatus(status="R", request=request)
        request_status_running.save()
        self.assertEqual(request.last_status_change, request_status_running.when)
        request_status_completed = LTARequestStatus(status="C", request=request)
        request_status_completed.save()
        self.assertEqual(request.last_status_change, request_status_completed.when)


class TestLTARequestErrors(TestCase):
    @patch('stageit.celery.app.send_task')
    def test_last_error(self, _):
        request = LTARequest(request_type='stage')
        request.save()

        lta_errors = [LTAError(generated_by=request, error="Dude really?") for _ in range(3)]
        for error in lta_errors:
            error.save()

        errors = request.last_errors
        for expected, returned in zip(lta_errors[::-1], errors):
            self.assertEqual(returned, expected)
        status = LTARequestStatus(status='P', request=request)
        status.save()
        self.assertEqual(len(request.last_errors), 0)

        lta_errors = [LTAError(generated_by=request, error="Dude really?") for _ in range(2)]
        for error in lta_errors:
            error.save()

        errors = request.last_errors
        for expected, returned in zip(lta_errors[::-1], errors):
            self.assertEqual(returned, expected)


class TestLTAMacaroons(TestCase):

    @patch('stageit.celery.app.send_task')
    def test_request_no_macaroon(self, _):
        """Test LTARequests with No LTAMacaroons"""
        lta_site = LTASite(name='test_site_one', surl_pattern='(srm://srm.grid.sara.nl:8443/.*)')
        lta_site.save()
        lta_request = LTARequest(request_type='stage')
        lta_request.save()
        # Call the LTArequest properties/methods to be tested -> no macaroons for LTARequest
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        self.assertEqual(lta_request.get_macaroon_from_site(lta_site), None)

    @patch('stageit.celery.app.send_task')
    def test_request_with_mutiple_macaroons(self, _):
        """Test that multiple LTAMacaroons can be associated with one LTARequest """
        lta_site = LTASite(name='test_site_one', valid_time=11)
        lta_site.save()
        lta_request = LTARequest(request_type='stage')
        lta_request.save()
        lta_macaroons = [LTAMacaroon(requested_by=lta_request, content="ILikeCookies", caveat_root_path="/pnfs/grid.sara.nl/data/lofar/ops/projects", lta_site=lta_site) for _ in range(3)]
        for mac in lta_macaroons:
            mac.save()
        time_saved = timezone.now()
        # Check that valid_until time is set (within current time plus the validation duration of the LTA site) of the macaroon objects
        for mac in lta_macaroons:
            self.assertAlmostEqual(mac.valid_until, time_saved, delta=timezone.timedelta(days=11, seconds=1))
        # Call the LTArequest properties/methods to be tested
        self.assertTrue(lta_request.has_macaroon)
        self.assertEqual(lta_request.macaroons.all().count(), 3)

    @patch('stageit.celery.app.send_task')
    def test_request_with_one_macaroon(self, _):
        """Test LTARequest with LTAMacaroon"""
        lta_site = LTASite(name='test_site_one', surl_pattern='(srm://srm.grid.sara.nl:8443/.*)', valid_time=11)
        lta_site.save()
        lta_request = LTARequest(request_type='stage')
        lta_request.save()
        lta_macaroon = LTAMacaroon(requested_by=lta_request, content="ILikeCookies", lta_site=lta_site)
        lta_macaroon.save()
        # Call the LTArequest properties/methods to be tested
        self.assertTrue(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, ['test_site_one'])
        self.assertEqual(lta_request.get_macaroon_from_site(lta_site).content, "ILikeCookies")

    @patch('stageit.celery.app.send_task')
    def test_macaroon_sites(self, _):
        """Test LTARequests with LTAMacaroons of different LTASites"""
        lta_site1 = LTASite(name='test_site_nl', surl_pattern='srm://srm.grid.sara.nl:8443/', valid_time=1)
        lta_site1.save()
        lta_site2 = LTASite(name='test_site_de', surl_pattern='srm://srm.grid.sara.de:8443/', valid_time=2)
        lta_site2.save()

        lta_request = LTARequest(request_type='stage')
        lta_request.save()
        lta_macaroon = LTAMacaroon(requested_by=lta_request, content="ILikeCookies", lta_site=lta_site1)
        lta_macaroon.save()

        # Call the LTArequest properties/methods to be tested
        self.assertTrue(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, ['test_site_nl'])
        self.assertEqual(lta_request.get_macaroon_from_site(lta_site1).content, "ILikeCookies")
        self.assertEqual(lta_request.get_macaroon_from_site(lta_site2), None)

        # add macaroon from another site
        lta_macaroon = LTAMacaroon(requested_by=lta_request, content="ILikeCookiesToo", lta_site=lta_site2)
        lta_macaroon.save()
        # Call the LTArequest properties/methods to be tested
        self.assertTrue(lta_request.has_macaroon)
        self.assertCountEqual(lta_request.lta_macaroon_sites, ['test_site_nl', 'test_site_de'])
        self.assertEqual(lta_request.get_macaroon_from_site(lta_site1).content, "ILikeCookies")
        self.assertEqual(lta_request.get_macaroon_from_site(lta_site2).content, "ILikeCookiesToo")
        # Check if surl has associated macaroon
        surl_has_macaroon = lta_request.macaroon_exists_for_surl("dummy_file")
        self.assertFalse(surl_has_macaroon)
        surl_has_macaroon = lta_request.macaroon_exists_for_surl("srm://srm.grid.sara.nl:8443/dummy_file")
        self.assertTrue(surl_has_macaroon)
        surl_has_macaroon = lta_request.macaroon_exists_for_surl("srm://srm.grid.sara.de:8443/dummy_file_two")
        self.assertTrue(surl_has_macaroon)