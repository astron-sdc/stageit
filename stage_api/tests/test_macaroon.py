import unittest.mock as mocker
import json
from django.test import TestCase
from django.utils import timezone

from django.contrib.auth.models import User
from stage_api.models.lta import LTARequest, LTARequestType, AffectedSurls, LTASite, LTAMacaroon
from stage_api.macaroon import *


class MockResponse:
    def __init__(self, json_data, status_code, content=None):
        self.json_data = json_data
        self.status_code = status_code
        if content is None:
            self.content = json.dumps(json_data)
        else:
            self.content = content
        if status_code < 400:
            self.ok = True
        else:
            self.ok = False

    def json(self):
        return self.json_data


# This method will be used by the mock to replace requests.post
def mocked_requests_post(*args, **kwargs):
    # if webdav defined then we are ok as response
    if 'my_webdav' in args[0]:
        mac_dict = {"macaroon": "ThisShouldBeAMacaroon"}
        if "sara" in args[0]:
            mac_dict['macaroon'] += "WithGreetingsFromSara"
        if "juelich" in args[0]:
            mac_dict['macaroon'] += "WithGreetingsFromJuelich"
        if "poznan" in args[0]:
            mac_dict['macaroon'] += "WithGreetingsFromPozan"
        return MockResponse(mac_dict, 200)
    # otherwise an error
    return MockResponse(None, 404)


SURLS_SINGlE_SITE = ['srm://my_lta_site/my_favourite_path/projects/LT1011/data/file.tar',
                     'srm://my_lta_site/my_favourite_path/projects/LT1_11/data/file.tar',
                     'srm://my_lta_site/my_favourite_path/projects/111111/data/file.tar',
                     'srm://my_lta_site/my_favourite_path/projects/dummy/data/file.tar',
                     'srm://my_lta_site/my_favourite_path/projects/lt10_10/data/file.tar']

SURLS_MULTIPLE_SITES = ["srm://srm.grid.sara.nl/projects/my_cool_file",
                        "srm://srm.grid.sara.nl/projects/my_second_file",
                        "srm://my_lta_site/my_favourite_path/projects/LT1011/data/file.tar",
                        "srm://my_lta_site/my_favourite_path/projects/LT1_11/data/file.tar",
                        "srm://lofar-srm.fz-juelich.de:8443/projects/my_cool_file"]


def _set_urls(request, surls):
    for url in surls:
        aff_surl = AffectedSurls(surl=url, request=request)
        aff_surl.save()
    return request, surls


def create_lta_request_single_site(user=User(username="tester")):
    user.save()
    surls = SURLS_SINGlE_SITE
    lta_site = LTASite(name='test_site_one', surl_pattern='srm://my_lta_site', webdav_base_url="https://my_webdav")
    lta_site.save()

    request = LTARequest(request_type=LTARequestType.STAGE, user=user)
    request.save()
    return _set_urls(request, surls)


def create_lta_request_multiple_sites():
    surls = SURLS_MULTIPLE_SITES
    lta_site1 = LTASite(name='test_site_my', surl_pattern='srm://my_lta_site', webdav_base_url="https://my_webdav/from_my_lta_site", valid_time=11)
    lta_site1.save()
    lta_site2 = LTASite(name='test_site_sara', surl_pattern='srm://srm.grid.sara.nl', webdav_base_url="https://my_webdav/from_sara", valid_time=22)
    lta_site2.save()
    lta_site3 = LTASite(name='test_site_juelich', surl_pattern='srm://lofar-srm.fz-juelich.de', webdav_base_url="https://my_webdav/from_juelich", valid_time=33)
    lta_site3.save()
    request = LTARequest(request_type=LTARequestType.STAGE)
    request.save()
    return _set_urls(request, surls)


def create_lta_request_with_wrong_lta_site():
    surls = SURLS_SINGlE_SITE
    lta_site = LTASite(name='test_site_wrong_path', surl_pattern='srm://wrong_lta_site_path', webdav_base_url="https://my_webdav")
    lta_site.save()
    request = LTARequest(request_type=LTARequestType.STAGE)
    request.save()
    return _set_urls(request, surls)


def create_lta_request_with_no_webdav_for_lta_site():
    surls = SURLS_SINGlE_SITE[:1]
    lta_site = LTASite(name='test_site_failure', surl_pattern='srm://my_lta_site')
    lta_site.save()
    request = LTARequest(request_type=LTARequestType.STAGE)
    request.save()
    return _set_urls(request, surls)


class TestStoreMacaroon(TestCase):

    # We patch 'requests.post' with our own method. The mock object is passed in to our test case method.
    @mocker.patch('requests.post', side_effect=mocked_requests_post)
    @mocker.patch('stage_api.macaroon.find_proxy')
    def test_macaroon_request_single_site(self, mock_find_proxy: mocker.MagicMock, mock_post: mocker.MagicMock):
        """Test handling of a macaroon of a request of surls from all the same LTA sites"""
        mock_find_proxy.return_value = "dummy path"
        lta_request, surls = create_lta_request_single_site()
        # Check currently no macaroons set for LTA request
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        # Call the method to be tested
        store_macaroon(lta_request, surls)
        # Check that macaroon is added to LTA request
        self.assertTrue(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, ['test_site_one'])
        self.assertEqual(lta_request.macaroons.all().count(), 1)
        # post request only called once
        self.assertEqual(mock_post.call_count, 1)

    # We patch 'requests.post' with our own method. The mock object is passed in to our test case method.
    @mocker.patch('requests.post', side_effect=mocked_requests_post)
    @mocker.patch('stage_api.macaroon.find_proxy')
    def test_macaroon_request_multiple_sites(self, mock_find_proxy: mocker.MagicMock, mock_post: mocker.MagicMock):
        """Test handling of a macaroon of a request of surls from all the same LTA sites"""
        mock_find_proxy.return_value = "dummy path"
        lta_request, surls = create_lta_request_multiple_sites()
        # Check currently no macaroons set for LTA request
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        # Call the method to be tested
        store_macaroon(lta_request, surls)
        # Check that macaroons are added to LTA request
        self.assertTrue(lta_request.has_macaroon)
        self.assertCountEqual(lta_request.lta_macaroon_sites, ['test_site_sara', 'test_site_my', 'test_site_juelich'])
        self.assertEqual(lta_request.macaroons.all().count(), 3)
        # Check the macaroons itself
        mac_sara = LTAMacaroon.objects.get(lta_site__name='test_site_sara')
        mac_my = LTAMacaroon.objects.get(lta_site__name='test_site_my')
        mac_julich = LTAMacaroon.objects.get(lta_site__name='test_site_juelich')
        self.assertEqual(mac_sara.content, "ThisShouldBeAMacaroonWithGreetingsFromSara")
        self.assertAlmostEqual(mac_sara.valid_until, timezone.now(), delta=timezone.timedelta(days=22),
                               msg="The validation date of 'Sara' macaroon is not within range")
        self.assertEqual(mac_my.content, "ThisShouldBeAMacaroon")
        self.assertAlmostEqual(mac_my.valid_until, timezone.now(), delta=timezone.timedelta(days=11),
                               msg="The validation date of 'My' macaroon My is not within range")
        self.assertEqual(mac_julich.content, "ThisShouldBeAMacaroonWithGreetingsFromJuelich")
        #print(LTAMacaroon.objects.get(lta_site__name='test_site_sara').valid_until)
        self.assertAlmostEqual(mac_julich.valid_until, timezone.now(), delta=timezone.timedelta(days=33),
                               msg="The validation date of 'Juelich' macaroon Juelich is not within range")
        # post request  called three times
        self.assertEqual(mock_post.call_count, 3)

    def test_macaroon_request_with_exception_no_lta_site(self):
        """Test raise of exception when surl does not belong to any LTA Site"""
        lta_request, surls = create_lta_request_with_wrong_lta_site()
        # Check currently no macaroons set for LTA request
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        # Call the method to be tested (exception)
        with self.assertRaises(StageITConfigurationError) as context:
            store_macaroon(lta_request, surls)
        self.assertIn("does not have an associated LTA Site.", str(context.exception))

    # We patch 'requests.post' with our own method. The mock object is passed in to our test case method.
    @mocker.patch('requests.post', side_effect=mocked_requests_post)
    @mocker.patch('stage_api.macaroon.find_proxy')
    def test_macaroon_request_with_error_response(self, mock_find_proxy: mocker.MagicMock, mock_post: mocker.MagicMock):
        """Test when response for macaroon request is incorrect, e.g. when webdav_url for the LTA Site is not configured"""
        lta_request, surls = create_lta_request_with_no_webdav_for_lta_site()
        # Check currently no macaroons set for LTA request
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        # Call the method to be tested (exception)
        store_macaroon(lta_request, surls)
        # Check no macaroons is set for LTA request since there was no webdav response OK
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        # Check if mocked_requests_post was NOT called since there is no url to call!
        mock_post.assert_not_called()

    def test_macaroon_request_for_skipped_user(self):
        """Test handling of a macaroon of a request of user 'admin' which will be skipped"""
        lta_request, surls = create_lta_request_single_site(user=User(username="admin"))
        # Check currently no macaroons set for LTA request
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        # Call the method to be tested
        store_macaroon(lta_request, surls)
        # Check that macaroon is NOT added to LTA request
        self.assertFalse(lta_request.has_macaroon)
        self.assertEqual(lta_request.lta_macaroon_sites, [])
        self.assertEqual(lta_request.macaroons.all().count(), 0)


class TestStoreMacaroonSubFunctions(TestCase):

    def test_generate_caveat_root_path(self):
        """Test generate of the root caveat path"""
        surl = "https://ltasite.com/some/path/projects/test/sasid/file"
        lta_site = mocker.MagicMock()
        lta_site.surl_pattern = "https://ltasite.com"
        # Call the method to be tested
        # the caveat root path is the path between the surl-pattern and 'projects' (with 'projects' included)
        result = generate_caveat_root_path(surl, lta_site)
        self.assertEqual(result, "/some/path/projects")

    def test_should_verify_certificate(self):
        """Test should verify """
        lta_site = mocker.MagicMock()
        lta_site.disable_certificate_check = False
        # Call the method to be tested
        result = should_verify_certificate(lta_site)
        self.assertTrue(result)
        # Case where certificate check is disabled
        lta_site.disable_certificate_check = True
        result = should_verify_certificate(lta_site)
        self.assertFalse(result)

    def test_create_caveats(self):
        """Test create caveats json output """
        caveat_root_path = "test/path/"
        valid_time = 7
        # Call the method to be tested
        result = create_caveats(caveat_root_path, valid_time)
        expected_caveats = {
            "caveats": ["root:test/path/", "activity:DOWNLOAD"],
            "validity": "P7D"
        }
        self.assertEqual(result, expected_caveats)

    @mocker.patch('requests.post')
    def test_send_macaroon_request(self, mock_post):
        """Test sending of macaroon request  """
        mock_response = mocker.MagicMock()
        mock_response.ok = True
        mock_response.status_code = 200
        mock_post.return_value = mock_response

        webdav_base_url = "https://anyltasite.com"
        caveats = {"caveats": ["root:/test", "activity:DOWNLOAD"], "validity": "P7D"}
        proxy_file_path = "/dummy/proxy"
        verify_certificate = True
        # Call the method to be tested
        response = send_macaroon_request(webdav_base_url, caveats, proxy_file_path, verify_certificate)
        # Check request post call
        mock_post.assert_called_once_with(
            webdav_base_url,
            headers={'Content-Type': 'application/macaroon-request'},
            data=json.dumps(caveats),
            cert=proxy_file_path,
            verify=verify_certificate
        )
        self.assertEqual(response.status_code, 200)

    @mocker.patch('stage_api.models.lta.LTAMacaroon.save')
    def test_save_macaroon(self, mock_save):
        """Test saving macaroon  """
        lta_request = LTARequest(request_type=LTARequestType.STAGE)
        macaroon_content = "test macaroon"
        caveat_root_path = "/test_path"
        lta_site = LTASite(name='test_site_one', surl_pattern='srm://my_lta_site', webdav_base_url="https://my_webdav")
        # Call the method to be tested and check if save is called
        save_macaroon(macaroon_content, lta_request, caveat_root_path, lta_site)
        mock_save.assert_called_once()

    @mocker.patch('logging.error')
    @mocker.patch('stage_api.macaroon.generate_caveat_root_path')
    @mocker.patch('stage_api.macaroon.find_proxy')
    @mocker.patch('stage_api.macaroon.should_verify_certificate')
    @mocker.patch('stage_api.macaroon.create_caveats')
    @mocker.patch('stage_api.macaroon.send_macaroon_request')
    @mocker.patch('stage_api.macaroon.save_macaroon')
    def test_generate_and_store_macaroon(self, mock_save_macaroon, mock_send_macaroon_request,
                                         mock_create_caveats, mock_should_verify_certificate,
                                         mock_find_proxy, mock_generate_caveat_root_path, mock_log):
        """Test that macaroon is saved when request for macaroon is ok and not saved when not ok"""
        mock_create_caveats.return_value = "my_caveats"
        mock_find_proxy.return_value = "my_proxy"
        mock_should_verify_certificate.return_value = True
        # Test with OK
        mock_send_macaroon_request.return_value.ok = True

        lta_request = mocker.MagicMock()
        surl = "https://anyltasite.com/test/file"
        lta_site = LTASite(name='test_site_one', surl_pattern='srm://my_lta_site', webdav_base_url="https://my_webdav", valid_time=99)
        # Call the method to be tested
        generate_and_store_macaroon(lta_request, surl, lta_site)
        # Check that request is send and saved
        mock_send_macaroon_request.assert_called_once_with("https://my_webdav", "my_caveats", "my_proxy", True)
        mock_save_macaroon.assert_called_once()
        mock_log.assert_not_called()

        # Test now with not OK
        mock_save_macaroon.reset_mock()
        mock_log.reset_mock()
        mock_send_macaroon_request.return_value.ok = False
        # Call the method to be tested
        generate_and_store_macaroon(lta_request, surl, lta_site)
        # Check that request is not send and saved but logging error instead (no exception)
        mock_save_macaroon.assert_not_called()
        mock_log.assert_called_once()

    @mocker.patch('stage_api.models.lta.LTASite.get_site_from_surl')
    @mocker.patch('stage_api.macaroon.generate_and_store_macaroon')
    def test_process_surl(self, mock_generate_and_store_macaroon, mock_get_site):
        """Test process surl when lta site and webdav_base url exist"""
        mock_get_site.return_value = mocker.MagicMock(webdav_base_url="https://anyltasite.com", valid_time=7)

        lta_request = mocker.MagicMock()
        lta_request.macaroon_exists_for_surl.return_value = False
        lta_request.id.return_value = 233
        surl = "https://anyltasite.com/test/file"
        # Call the method to be tested
        process_surl(lta_request, surl)
        mock_generate_and_store_macaroon.assert_called_once()

    @mocker.patch('stage_api.models.lta.LTASite.get_site_from_surl')
    @mocker.patch('stage_api.macaroon.generate_and_store_macaroon')
    def test_process_surl(self, mock_generate_and_store_macaroon, mock_get_site):
        """Test process surl when lta site and webdav_base url exist"""
        mock_get_site.return_value = mocker.MagicMock(webdav_base_url="https://anyltasite.com", valid_time=7)
        lta_request = mocker.MagicMock()
        lta_request.macaroon_exists_for_surl.return_value = False
        surl = "https://anyltasite.com/test/file"
        # Call the method to be tested
        process_surl(lta_request, surl)
        mock_generate_and_store_macaroon.assert_called_once()

    @mocker.patch('stage_api.models.lta.LTASite.get_site_from_surl')
    @mocker.patch('stage_api.macaroon.generate_and_store_macaroon')
    def test_process_surl_invalid_lta_site(self, mock_generate_and_store_macaroon, mock_get_site):
        """Test process surl when lta site does not exist gives an Exception (should not happen)"""
        mock_get_site.return_value = None # No LTA Site
        lta_request = mocker.MagicMock()
        lta_request.macaroon_exists_for_surl.return_value = False
        surl = "https://anyltasite.com/test/file"
        # Call the method to be tested
        with self.assertRaises(StageITConfigurationError) as context:
            process_surl(lta_request, surl)
        self.assertIn("does not have an associated LTA Site.", str(context.exception))

    @mocker.patch('stage_api.models.lta.LTASite.get_site_from_surl')
    @mocker.patch('stage_api.macaroon.generate_and_store_macaroon')
    def test_process_surl_no_webdav_defined(self, mock_generate_and_store_macaroon, mock_get_site):
        """Test process surl when lta site does have a webdav url defined """
        mock_get_site.return_value = mocker.MagicMock(webdav_base_url="", valid_time=7)
        lta_request = mocker.MagicMock()
        lta_request.macaroon_exists_for_surl.return_value = False
        surl = "https://anyltasite.com/test/file"
        # Call the method to be tested
        process_surl(lta_request, surl)
        mock_generate_and_store_macaroon.assert_not_called()

    @mocker.patch('stage_api.models.lta.LTASite.get_site_from_surl')
    @mocker.patch('stage_api.macaroon.generate_and_store_macaroon')
    def test_process_surl_already_exist(self, mock_generate_and_store_macaroon, mock_get_site):
        """Test process surl when macaroon for surl already exist"""
        mock_get_site.return_value = mocker.MagicMock(webdav_base_url="https://anyltasite.com", valid_time=7)
        lta_request = mocker.MagicMock()
        lta_request.id.return_value = 233
        lta_request.macaroon_exists_for_surl.return_value = True
        surl = "https://anyltasite.com/test/file"
        # Call the method to be tested
        process_surl(lta_request, surl)
        mock_generate_and_store_macaroon.assert_not_called()

    @mocker.patch('logging.info')
    @mocker.patch('stage_api.macaroon.process_surl')
    def test_store_macaroon_admin_user(self, mock_process_surl, mock_log):
        """Test that macaroon is not stored in case of admin user """
        lta_request = mocker.MagicMock()
        lta_request.user.username = "admin"
        surls = ["https://surl1", "https://surl2", "https://surl3"]
        # Call the method to be tested
        store_macaroon(lta_request, surls)
        # Check that the macaroon is not processed, only logging
        mock_log.assert_called_once_with("No need to create macaroon")
        mock_process_surl.assert_not_called()

    @mocker.patch('logging.info')
    @mocker.patch('stage_api.macaroon.process_surl')
    def test_store_macaroon_non_admin_user(self, mock_process_surl, mock_log):
        """Test that macaroon is stored in case of non-admin user """
        lta_request = mocker.MagicMock()
        lta_request.user.username = "non_admin"
        surls = ["https://surl1", "https://surl2", "https://surl3"]
        # Call the method to be tested
        store_macaroon(lta_request, surls)
        # Check that the macaroon is processed
        mock_process_surl.assert_called()
        self.assertEqual(mock_process_surl.call_count, 3)
        mock_log.assert_not_called()


