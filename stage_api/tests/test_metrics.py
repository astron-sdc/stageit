import unittest
from unittest.mock import patch, MagicMock
from stage_api.models.lta import LTARequest, LTARequestType, LTASite, LTARequestStatus, AffectedSurls, LTARequestStatusCode
from django.test import TestCase
from stage_api.metrics import StageITCustomCollector
from prometheus_client.core import GaugeMetricFamily


def _set_urls(request, surls, size_per_url):
    for url in surls:
        aff_surl = AffectedSurls(surl=url, request=request, size=size_per_url)
        aff_surl.save()
    return request, surls

TEST_SURLS = ["srm://een", "srm://twee", "srm://drie", "srm://vier", "srm://vijf", "srm://zes", "srm://zeven"]


def show_metrics(metric):
    """For debug purposes: a way to show the values"""
    print(f"{metric.name} has {len(metric.samples)} samples")
    for idx in range(len(metric.samples)):
        sample = metric.samples[idx]
        print(f"Metric: {sample.name} Labels: {sample.labels} Value: {sample.value}")


class TestMetrics(TestCase):
    """
    Perform testcases on metrics update functions with setup with test data in datamodel (LTA request, LTASite, AffectedUrls)
    """

    def test_update_lta_stage_requests_gauge_metrics(self):
        # Create test data for LTARequest
        lta_request1 = LTARequest.objects.create(current_status_code='P', request_type=LTARequestType.STAGE)
        lta_request2 = LTARequest.objects.create(current_status_code='R', request_type=LTARequestType.STAGE)

        lta_request_status = LTARequestStatus(status_code='P', request=lta_request1)
        lta_request_status.save()
        lta_request_status = LTARequestStatus(status_code='R', request=lta_request2)
        lta_request_status.save()

        collector = StageITCustomCollector()
        # Function to test
        gauge_result = collector.create_lta_stage_requests_gauge()
        #show_metrics(gauge_result)
        # Check that the gauge has been updated correctly
        self.assertEqual(gauge_result.name, "stageit_lta_stage_requests_active")
        self.assertIsInstance(gauge_result, GaugeMetricFamily)
        self.assertEqual(len(gauge_result.samples), len(LTARequestStatusCode.choices))  # its looping
        for idx in range(len(LTARequestStatusCode.choices)):
            code, text = LTARequestStatusCode.choices[idx]
            self.assertEqual(gauge_result.samples[idx].labels['status_code'], code, msg="Incorrect LTARequestStateCode code")
            self.assertEqual(gauge_result.samples[idx].labels['status_text'], text, msg="Incorrect LTARequestStateCode text")
            if code in ["P", "R"]:
                expected_value = 1
            else:
                expected_value = 0
            self.assertEqual(gauge_result.samples[idx].value, expected_value)

    def test_update_lta_site_files_gauge_metrics(self):
        # Create test data for LTASite
        site1 = LTASite(name='Site1')
        site1.save()
        site2 = LTASite(name='Site2')
        site2.save()

        # Create test data for LTARequest with retry counts
        request1 = LTARequest(retry_count=10, request_type=LTARequestType.STAGE,)
        request1.save()
        request1.lta_sites.set([site1])
        _set_urls(request1, TEST_SURLS[1:5], 100)
        request2 = LTARequest(retry_count=20, request_type=LTARequestType.STAGE)
        request2.save()
        request2.lta_sites.set([site2])
        _set_urls(request2, TEST_SURLS, 200)

        collector = StageITCustomCollector()
        # Function to test
        gauge_result = collector.create_lta_site_files_gauge()
        #show_metrics(gauge_result)
        # Check that the gauge has been updated correctly
        self.assertIsInstance(gauge_result, GaugeMetricFamily)
        self.assertEqual(gauge_result.name, "stageit_lta_site_total_scheduled_files")
        self.assertEqual(len(gauge_result.samples), 8)
        self.assertEqual(gauge_result.samples[0].labels['lta_site'], 'Site1')
        self.assertEqual(gauge_result.samples[0].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[0].value, 4)
        self.assertEqual(gauge_result.samples[1].labels['lta_site'], 'Site1')
        self.assertEqual(gauge_result.samples[1].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[1].value, 400)
        self.assertEqual(gauge_result.samples[2].labels['lta_site'], 'Site1')
        self.assertEqual(gauge_result.samples[2].labels['type'], 'total_retries')
        self.assertEqual(gauge_result.samples[2].value, 10)
        self.assertEqual(gauge_result.samples[3].labels['lta_site'], 'Site1')
        self.assertEqual(gauge_result.samples[3].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[3].value, 1)

        self.assertEqual(gauge_result.samples[4].labels['lta_site'], 'Site2')
        self.assertEqual(gauge_result.samples[4].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[4].value, 7)
        self.assertEqual(gauge_result.samples[5].labels['lta_site'], 'Site2')
        self.assertEqual(gauge_result.samples[5].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[5].value, 1400)
        self.assertEqual(gauge_result.samples[6].labels['lta_site'], 'Site2')
        self.assertEqual(gauge_result.samples[6].labels['type'], 'total_retries')
        self.assertEqual(gauge_result.samples[6].value, 20)
        self.assertEqual(gauge_result.samples[7].labels['lta_site'], 'Site2')
        self.assertEqual(gauge_result.samples[7].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[7].value, 1)


class TestMetricsSimple(TestCase):
    """
    Perform testcases on metrics update functions with mocking the 'utils' functions
    """

    @patch('stage_api.models.lta.LTARequest.objects.filter')
    def test_create_lta_stage_requests_gauge_metrics(self, mock_filter):
        # we have 7 states defines
        mock_filter.return_value.count.side_effect = [11, 12, 13, 14, 15, 16, 17]

        collector = StageITCustomCollector()
        # Function to test
        gauge_result = collector.create_lta_stage_requests_gauge()
        #show_metrics(gauge_result)
        # Check that the gauge has been updated correctly
        self.assertEqual(gauge_result.name, "stageit_lta_stage_requests_active")
        self.assertIsInstance(gauge_result, GaugeMetricFamily)
        self.assertEqual(len(gauge_result.samples), len(LTARequestStatusCode.choices))  # its looping
        for idx in range(len(LTARequestStatusCode.choices)):
            code, text = LTARequestStatusCode.choices[idx]
            self.assertEqual(gauge_result.samples[idx].labels['status_code'], code, msg="Incorrect LTARequestStateCode code")
            self.assertEqual(gauge_result.samples[idx].labels['status_text'], text, msg="Incorrect LTARequestStateCode text")
            self.assertEqual(gauge_result.samples[idx].value, 11+idx)


    @patch('stage_api.utils.get_total_surl_size_and_retry_count_per_lta_site')
    def test_create_lta_site_files_gauge_metrics(self, get_total_surl_size_and_retry_count_per_lta_site_mock: unittest.mock.MagicMock):
        get_total_surl_size_and_retry_count_per_lta_site_mock.return_value = \
            {'SURF': {'total_surls': 8, 'total_size': 800, 'total_retries': 3, 'total_request_ids': 2},
             'Juelich': {'total_surls': 7, 'total_size': 700, 'total_retries': 5, 'total_request_ids': 3}}

        collector = StageITCustomCollector()
        # Function to test
        gauge_result = collector.create_lta_site_files_gauge()
        #show_metrics(gauge_result)
        # Check that the gauge has been updated correctly
        self.assertIsInstance(gauge_result, GaugeMetricFamily)
        self.assertEqual(gauge_result.name, "stageit_lta_site_total_scheduled_files")
        self.assertEqual(len(gauge_result.samples), 8)
        self.assertEqual(gauge_result.samples[0].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[0].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[0].value, 8)
        self.assertEqual(gauge_result.samples[1].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[1].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[1].value, 800)
        self.assertEqual(gauge_result.samples[2].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[2].labels['type'], 'total_retries')
        self.assertEqual(gauge_result.samples[2].value, 3)
        self.assertEqual(gauge_result.samples[3].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[3].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[3].value, 2)

        self.assertEqual(gauge_result.samples[4].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[4].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[4].value, 7)
        self.assertEqual(gauge_result.samples[5].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[5].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[5].value, 700)
        self.assertEqual(gauge_result.samples[6].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[6].labels['type'], 'total_retries')
        self.assertEqual(gauge_result.samples[6].value, 5)
        self.assertEqual(gauge_result.samples[7].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[7].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[7].value, 3)

    @patch('stage_api.utils.get_total_surl_size_and_retry_count_per_user')
    def test_create_lta_user_files_gauge_metrics(self, get_total_surl_size_and_retry_count_per_user_mock: unittest.mock.MagicMock):
        get_total_surl_size_and_retry_count_per_user_mock.return_value = \
            {1245: {'total_surls': 8, 'total_size': 800, 'total_retries': 3, 'total_request_ids': 1},
             5678: {'total_surls': 7, 'total_size': 700, 'total_retries': 5, 'total_request_ids': 2}}

        collector = StageITCustomCollector()
        # Function to test
        gauge_result = collector.create_lta_user_files_gauge()
        #show_metrics(gauge_result)
        # Check that the gauge has been updated correctly
        self.assertIsInstance(gauge_result, GaugeMetricFamily)
        self.assertEqual(gauge_result.name, "stageit_lta_user_total_scheduled_files")
        self.assertEqual(len(gauge_result.samples), 8)
        self.assertEqual(gauge_result.samples[0].labels['user'], '1245')
        self.assertEqual(gauge_result.samples[0].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[0].value, 8)
        self.assertEqual(gauge_result.samples[1].labels['user'], '1245')
        self.assertEqual(gauge_result.samples[1].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[1].value, 800)
        self.assertEqual(gauge_result.samples[2].labels['user'], '1245')
        self.assertEqual(gauge_result.samples[2].labels['type'], 'total_retries')
        self.assertEqual(gauge_result.samples[2].value, 3)
        self.assertEqual(gauge_result.samples[3].labels['user'], '1245')
        self.assertEqual(gauge_result.samples[3].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[3].value, 1)

        self.assertEqual(gauge_result.samples[4].labels['user'], '5678')
        self.assertEqual(gauge_result.samples[4].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[4].value, 7)
        self.assertEqual(gauge_result.samples[5].labels['user'], '5678')
        self.assertEqual(gauge_result.samples[5].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[5].value, 700)
        self.assertEqual(gauge_result.samples[6].labels['user'], '5678')
        self.assertEqual(gauge_result.samples[6].labels['type'], 'total_retries')
        self.assertEqual(gauge_result.samples[6].value, 5)
        self.assertEqual(gauge_result.samples[7].labels['user'], '5678')
        self.assertEqual(gauge_result.samples[7].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[7].value, 2)


    @patch('stage_api.utils.get_total_surl_size_per_retry_count_per_lta_site')
    def test_create_lta_retry_numbers_gauge_metrics(self, get_total_surl_size_per_retry_count_per_lta_site_mock: unittest.mock.MagicMock):

        get_total_surl_size_per_retry_count_per_lta_site_mock.return_value = \
                           {"SURF": {
                                     1: {"total_surls": 3,
                                         "total_size": 300,
                                         "total_request_ids": 1},
                                     2: {"total_surls": 5,
                                         "total_size": 500,
                                         "total_request_ids": 1
                                         }},
                           "Juelich": {
                                     1: {"total_surls": 2,
                                         "total_size": 200,
                                         "total_request_ids": 1},
                                     2: {"total_surls": 5,
                                         "total_size": 500,
                                         "total_request_ids": 2}}}

        collector = StageITCustomCollector()
        # Function to test
        gauge_result = collector.create_lta_retry_numbers_gauge()
        # Check that the gauge has been updated correctly
        self.assertIsInstance(gauge_result, GaugeMetricFamily)
        self.assertEqual(gauge_result.name, "stageit_lta_retry_numbers")
        self.assertEqual(len(gauge_result.samples), 12)
        self.assertEqual(gauge_result.samples[0].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[0].labels['retry_count'], '1')
        self.assertEqual(gauge_result.samples[0].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[0].value, 3)
        self.assertEqual(gauge_result.samples[1].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[1].labels['retry_count'], '1')
        self.assertEqual(gauge_result.samples[1].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[1].value, 300)
        self.assertEqual(gauge_result.samples[2].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[2].labels['retry_count'], '1')
        self.assertEqual(gauge_result.samples[2].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[2].value, 1)

        self.assertEqual(gauge_result.samples[3].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[3].labels['retry_count'], '2')
        self.assertEqual(gauge_result.samples[3].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[3].value, 5)
        self.assertEqual(gauge_result.samples[4].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[4].labels['retry_count'], '2')
        self.assertEqual(gauge_result.samples[4].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[4].value, 500)
        self.assertEqual(gauge_result.samples[5].labels['lta_site'], 'SURF')
        self.assertEqual(gauge_result.samples[5].labels['retry_count'], '2')
        self.assertEqual(gauge_result.samples[5].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[5].value, 1)

        self.assertEqual(gauge_result.samples[6].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[6].labels['retry_count'], '1')
        self.assertEqual(gauge_result.samples[6].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[6].value, 2)
        self.assertEqual(gauge_result.samples[7].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[7].labels['retry_count'], '1')
        self.assertEqual(gauge_result.samples[7].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[7].value, 200)
        self.assertEqual(gauge_result.samples[8].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[8].labels['retry_count'], '1')
        self.assertEqual(gauge_result.samples[8].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[8].value, 1)

        self.assertEqual(gauge_result.samples[9].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[9].labels['retry_count'], '2')
        self.assertEqual(gauge_result.samples[9].labels['type'], 'total_surls')
        self.assertEqual(gauge_result.samples[9].value, 5)
        self.assertEqual(gauge_result.samples[10].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[10].labels['retry_count'], '2')
        self.assertEqual(gauge_result.samples[10].labels['type'], 'total_size')
        self.assertEqual(gauge_result.samples[10].value, 500)
        self.assertEqual(gauge_result.samples[11].labels['lta_site'], 'Juelich')
        self.assertEqual(gauge_result.samples[11].labels['retry_count'], '2')
        self.assertEqual(gauge_result.samples[11].labels['type'], 'total_request_ids')
        self.assertEqual(gauge_result.samples[11].value, 2)
