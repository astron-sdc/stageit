from unittest.mock import patch

import django.test
from django.test import TestCase

from stage_api.models.lta import LTARequest, AffectedSurls, LTARequestStatus


@django.test.override_settings(CELERY_TASK_ALWAYS_EAGER=True)
@patch('stageit.celery.app.send_task')
class TestFixRequest(TestCase):

    def setUp(self):
        self.surls = ['srm://url1', 'srm://url2']

    def test_request_without_info(self, _):
        request = LTARequest()
        request.save()
        for url in self.surls:
            aff_surl = AffectedSurls(surl=url, request=request)
            aff_surl.save()

        LTARequestStatus(status_code='P', request=request).save()
        LTARequestStatus(status_code='C', request=request).save()
        request = LTARequest.objects.get(pk=request.pk)
        self.assertEqual(request.response, {})

        request.update()
        request = LTARequest.objects.get(pk=request.pk)
        self.assertEqual(request.current_status_code, 'C')
        # Check contents of the online dict (since order can't be fixed)

        self.assertEqual(set(request.response["online"]), set(self.surls))
        request.response.pop("online")
        self.assertDictEqual(request.response, {'errors': [], 'progressing': [], 'n_errors': 0, 'n_progressing': 0,
                                                'n_online': len(self.surls), 'n_finished': len(self.surls)})

    def test_request_with_error(self, _):
        request = LTARequest()
        request.save()
        for url in self.surls:
            aff_surl = AffectedSurls(surl=url, request=request)
            aff_surl.save()

        LTARequestStatus(status_code='P', request=request).save()
        LTARequestStatus(status_code='E', request=request).save()

        request = LTARequest.objects.get(pk=request.pk)
        self.assertEqual(request.response, {})

        request.update()
        request = LTARequest.objects.get(pk=request.pk)
        self.assertEqual(request.current_status_code, 'E')
        self.assertDictEqual(request.response, {})
