import errno
import types
import unittest
from dcacheclient.rest import Locality

from stage_api.lta_utils import evaluate_errors_bring_online

class TestEvaluateErrorsBringOnline(unittest.TestCase):

    def setUp(self):
        self.empty_result = {
            'errors': {},
            'progressing': [],
            'online': [],
            'n_errors': 0,
            'n_progressing': 0,
            'n_online': 0,
            'n_finished': 0
        }

        self.errors = {
            -1: types.SimpleNamespace(code=-1, message='boom'),
            -2: types.SimpleNamespace(code=-2, message='blammo'),
            errno.EAGAIN: types.SimpleNamespace(code=errno.EAGAIN, message='kaboom')
        }

    def test_empty(self):
        actual = evaluate_errors_bring_online([], [])
        self.assertEqual(actual, self.empty_result)

    def test_finished_count(self):
        surls = ['surl1', 'surl2', 'surl3', 'surl4', 'surl5']
        errors = [
            '',
            Locality.ONLINE_AND_NEARLINE.value, # +1
            self.errors[-1], # +1
            '',
            {} # +1
        ]

        actual = evaluate_errors_bring_online(surls, errors)
        self.assertEqual(actual['n_finished'], 3)

    def test_online_count(self):
        surls = ['surl6', 'surl7', 'surl8', 'surl9', 'surl10']
        errors = [
            Locality.ONLINE_AND_NEARLINE.value, # +1,
            '',
            self.errors[-1],
            {}, # +1
            ''
        ]

        actual = evaluate_errors_bring_online(surls, errors)
        self.assertEqual(actual['n_online'], 2)

    def test_progressing_count(self):
        surls = ['surl11', 'surl12', 'surl13', 'surl14', 'surl15', 'surl16', 'surl17', 'surl18']
        errors = [
            self.errors[-1],
            Locality.ONLINE_AND_NEARLINE.value,
            Locality.NEARLINE.value, # +1
            Locality.NEARLINE.value, # +1
            '',
            {},
            self.errors[-1],
            self.errors[errno.EAGAIN] # +1
        ]

        actual = evaluate_errors_bring_online(surls, errors)
        self.assertEqual(actual['n_progressing'], 3)

    def test_error_count(self):
        surls = ['surl21', 'surl22', 'surl23', 'surl24', 'surl25', 'surl26', 'surl27']
        errors = [
            self.errors[-1], # +1
            Locality.ONLINE_AND_NEARLINE.value,
            Locality.NEARLINE.value,
            'test',
            {},
            self.errors[-2], # + 1
            self.errors[errno.EAGAIN]
        ]

        actual = evaluate_errors_bring_online(surls, errors)
        self.assertEqual(actual['n_errors'], 2)

    def test_online(self):
        surls = ['surl55', 'surl56', 'surl57', 'surl58', 'surl59']
        errors = [
            '',
            Locality.ONLINE_AND_NEARLINE.value,  # +add,
            self.errors[-1],
            '',
            {},  # add
        ]

        actual = evaluate_errors_bring_online(surls, errors)
        expected = ['surl56', 'surl59']
        self.assertListEqual(actual['online'], expected)

    def test_progressing(self):
        surls = ['surl98', 'surl99', 'surl100', 'surl101', 'surl105', 'surl106', 'surl107', 'surl110']
        errors = [
            self.errors[-1],
            Locality.ONLINE_AND_NEARLINE.value,
            '',
            Locality.NEARLINE.value,  # add
            Locality.NEARLINE.value,  # add
            {},
            self.errors[errno.EAGAIN],  # add
            self.errors[-1],
        ]

        actual = evaluate_errors_bring_online(surls, errors)
        expected = ['surl101', 'surl105', 'surl107']
        self.assertListEqual(actual['progressing'], expected)

    def test_errors(self):
        surls = ['surl30', 'surl31', 'surl33', 'surl35', 'surl37', 'surl38', 'surl39']
        errors = [
            Locality.ONLINE_AND_NEARLINE.value,
            Locality.NEARLINE.value,
            self.errors[-1],  # add
            'test',
            {},
            self.errors[errno.EAGAIN],
            self.errors[-2],  # add
        ]

        actual = evaluate_errors_bring_online(surls, errors)
        expected = {
            'surl33': self.errors[-1].message,
            'surl39': self.errors[-2].message
        }
        self.assertDictEqual(actual['errors'], expected)