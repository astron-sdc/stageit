import django.test as dtest
import rest_framework.test as rtest
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.template.response import TemplateResponse
from django.urls import reverse
from rest_framework import status

from stage_api.models.lta import LTARequest, LTASite, LTARequestType, AffectedSurls


# TODO The LTA Requests are available via multiple APIs:
# - Django Forms (mainly for interaction via the GUI)
# - Django REST Framework (mainly for creating draft Requests via LTA GUI)
# - GraphQL (stager cli) (Read Only, not tested)
#
# This results in using both the Django test framework (testing the form methods)
# and using the APIClient from rest_framework. They differ mainly in way credentials
# are provided (force_login vs force_authentication)
#
# CommonSetup is used, to prevent DRY on creating the LTA Sites and users

class CommonSetup:
    def setUp(self) -> None:
        # Sets up the 3 LTA Sites
        LTASite.objects.create(name="SURFSara", surl_pattern="srm://srm.grid.sara.nl:8443")
        LTASite.objects.create(name="Juelich", surl_pattern="srm://lofar-srm.fz-juelich.de:8443")
        LTASite.objects.create(name="Poznan", surl_pattern="srm://lta-head.lofar.psnc.pl:8443")

        self.user = get_user_model().objects.create(username="test_user")
        self.admin = get_user_model().objects.create(username="admin", is_superuser=True, is_staff=True)


class TestFormRequestLTASited(CommonSetup, dtest.TestCase):

    def test_create_request(self):
        self.client.force_login(user=self.user)
        data = {"surls": "srm://srm.grid.sara.nl:8443/my/fancy/file\nsrm://srm.grid.sara.nl:8443/password.txt"}

        res = self.client.post(reverse("insert-request"), data=data)
        self.assertEqual(res.status_code, status.HTTP_302_FOUND, "could not create request")

        lta_site = LTASite.objects.filter(name="SURFSara")
        lta_request = LTARequest.objects.first()
        self.assertEqual(1, lta_request.lta_sites.count(), "invalid amount of LTA Sites")
        self.assertQuerysetEqual(lta_site, lta_request.lta_sites.all(), ordered=False, msg="invalid LTA Site")

    def test_multiple_sites(self):
        self.client.force_login(user=self.user)
        data = {
            "surls": "srm://srm.grid.sara.nl:8443/my/fancy/file\nsrm://lofar-srm.fz-juelich.de:8443/storage/bicycle"}

        res = self.client.post(reverse("insert-request"), data=data)
        self.assertEqual(res.status_code, status.HTTP_302_FOUND, "could not create request")

        lta_site = LTASite.objects.filter(name__in=["SURFSara", "Juelich"])
        lta_request = LTARequest.objects.first()
        self.assertEqual(2, lta_request.lta_sites.count(), "invalid amount of LTA Sites")
        self.assertQuerysetEqual(lta_site, lta_request.lta_sites.all(), ordered=False, msg="invalid LTA Sites")

    def test_view_LTASite(self):
        self.client.force_login(user=self.user)
        data = {"surls": "srm://srm.grid.sara.nl:8443/my/fancy/file\nsrm://srm.grid.sara.nl:8443/password.txt"}

        res = self.client.post(reverse("insert-request"), data=data)
        self.assertEqual(res.status_code, status.HTTP_302_FOUND, "could not create request")

        res: TemplateResponse = self.client.get(reverse("dashboard"))
        self.assertInHTML("<span>SURFSara</span>", res.content.decode())

    def test_view_LTASite_multiple(self):
        self.client.force_login(user=self.user)
        data = {
            "surls": "srm://srm.grid.sara.nl:8443/my/fancy/file\nsrm://lofar-srm.fz-juelich.de:8443/storage/bicycle"}

        res = self.client.post(reverse("insert-request"), data=data)
        self.assertEqual(res.status_code, status.HTTP_302_FOUND, "could not create request")
        res: TemplateResponse = self.client.get(reverse("dashboard"))
        # self.assertInHTML("<span>SURFSara</span><span>,</span><span>Juelich</span>", res.content.decode())
        self.assertInHTML("<span>SURFSara</span>", res.content.decode())
        self.assertInHTML("<span>Juelich</span>", res.content.decode())


class TestAPIRequestLTASite(CommonSetup, rtest.APITestCase):
    """Tests the API Endpoints handling adding the LTA Site correctly"""

    def test_create_request_rest(self):
        self.client.force_authenticate(user=self.admin)
        data = {"surls": ["srm://srm.grid.sara.nl:8443/my/fancy/file", "srm://srm.grid.sara.nl:8443/password.txt"],
            "request_type": LTARequestType.STAGE}
        res = self.client.post(reverse("LTARequest-list"), data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, res.status_code, f"could not create draft request: {res.data}")

        lta_request = LTARequest.objects.get(pk=res.data["id"])
        lta_site = LTASite.objects.filter(name="SURFSara")
        self.assertEqual(1, lta_request.lta_sites.count(), "invalid amount of lta sites")
        self.assertQuerysetEqual(lta_site, lta_request.lta_sites.all(), ordered=False, msg="invalid LTA Site")

    def test_create_request_from_draft(self):
        self.client.force_authenticate(user=self.admin)
        data = {"surls": ["srm://srm.grid.sara.nl:8443/my/fancy/file", "srm://srm.grid.sara.nl:8443/password.txt"],
            "request_type": LTARequestType.STAGE}
        res = self.client.post(reverse("draftltarequest-list"), data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, res.status_code, f"could not create draft request: {res.data}")

        draft_req = res.data
        self.client.force_authenticate(user=self.user)
        res = self.client.get(reverse("draftltarequest-claim", kwargs={"pk": draft_req["id"]}), follow=True)
        self.assertEqual(status.HTTP_200_OK, res.status_code, f"could not claim draft request: {res.data}")

        req = res.data
        lta_request = LTARequest.objects.get(pk=req["id"])
        lta_site = LTASite.objects.filter(name="SURFSara")
        self.assertEqual(1, lta_request.lta_sites.count(), "invalid amount of lta sites")
        self.assertQuerysetEqual(lta_site, lta_request.lta_sites.all(), ordered=False, msg="invalid LTA Site")


SINGLE_SITES = [["srm://srm.grid.sara.nl:8443/my_cool_file", "srm://srm.grid.sara.nl:8443/my_second_file"],
    ["srm://lofar-srm.fz-juelich.de:8443/my_cool_file", "srm://lofar-srm.fz-juelich.de:8443/my_second_file"],
    ["srm://srm.grid.sara.nl:8443/my_cool_file", "srm://srm.grid.sara.nl:8443/my_second_file"],
    ["srm://lta-head.lofar.psnc.pl:8443/my_cool_file", "srm://lta-head.lofar.psnc.pl:8443/my_second_file"], ]

MULTIPLE_SITES = [["srm://srm.grid.sara.nl:8443/my_cool_file", "srm://srm.grid.sara.nl:8443/my_second_file"],
    ["srm://lofar-srm.fz-juelich.de:8443/my_cool_file", "srm://lta-head.lofar.psnc.pl:8443/my_second_file"],
    ["srm://srm.grid.sara.nl:8443/my_second_file", "srm://srm.grid.sara.nl:8443/my_cool_file",
        "srm://srm.grid.sara.nl:8443/my_second_file", "srm://lofar-srm.fz-juelich.de:8443/my_cool_file",
        "srm://lta-head.lofar.psnc.pl:8443/my_cool_file", "srm://lta-head.lofar.psnc.pl:8443/my_second_file"], ]


class TestCommandRequestLTASite(CommonSetup, dtest.TestCase):
    """Test the django command updating the requests correctly"""

    def test_run_command_single_sites(self):
        for i in range(20):
            req = LTARequest.objects.create(request_type=LTARequestType.STAGE)
            idx = i % len(SINGLE_SITES)
            for j in SINGLE_SITES[idx]:
                AffectedSurls.objects.create(surl=j, request=req)

        args = []
        opts = {}
        call_command("set_ltarequest_site", *args, **opts)

        for req in LTARequest.objects.all():
            self.assertEqual(1, req.lta_sites.count())

    def test_run_command_multiple_sites(self):

        reqs = []

        for i in range(20):
            idx = i % len(MULTIPLE_SITES)
            req = LTARequest.objects.create(request_type=LTARequestType.STAGE)
            reqs.append((idx + 1, req))
            for j in MULTIPLE_SITES[idx]:
                AffectedSurls.objects.create(surl=j, request=req)

        args = []
        opts = {}
        call_command("set_ltarequest_site", *args, **opts)

        for count, req in reqs:
            self.assertEqual(count, req.lta_sites.count())

    def test_run_command_no_surls(self):

        for i in range(20):
            req = LTARequest.objects.create(request_type=LTARequestType.STAGE)

        args = []
        opts = {}
        call_command("set_ltarequest_site", *args, **opts)

        for req in LTARequest.objects.all():
            self.assertEqual(0, req.lta_sites.count())
