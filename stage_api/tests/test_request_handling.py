import unittest.mock
from unittest.mock import patch

import django.test
from django.test import TestCase

from stage_api.models.lta import LTARequest, AffectedSurls
from stage_api.tasks import stage_surl_list


@django.test.override_settings(CELERY_TASK_ALWAYS_EAGER=True)
@patch('stageit.celery.app.send_task')
class TestFixRequest(TestCase):

    def setUp(self):
        self.surls = ['srm://url1', 'srm://url2']
        self.request = LTARequest(request_type='stage')
        self.request.save()
        for url in self.surls:
            aff_surl = AffectedSurls(surl=url, request=self.request)
            aff_surl.save()

    @patch('stage_api.lta_utils.size_of_file')
    @patch('stage_api.tasks.lta_utils.bring_online')
    @patch('stage_api.tasks.store_macaroon')
    def test_request_without_info(self, mocked_store_macaroon: unittest.mock.MagicMock,
                                  mocked_bring_online: unittest.mock.MagicMock,
                                  mocked_size_of_file: unittest.mock.MagicMock, _):
        mocked_bring_online.return_value = 'C', ''
        mocked_size_of_file.return_value = 0
        stage_surl_list(self.request.pk)
        mocked_bring_online.assert_called()
        mocked_store_macaroon.assert_called()
        request = LTARequest.objects.get(pk=self.request.pk)
        self.assertEqual(request.response,
                         {'errors': [], 'progressing': [], 'online': self.surls, 'n_errors': 0, 'n_progressing': 0,
                          'n_online': len(self.surls), 'n_finished': len(self.surls)})
