from typing import List

from rest_framework.serializers import ModelSerializer, StringRelatedField, ValidationError, ListField, CharField

from stage_api.models.draft import DraftLTARequest
from stage_api.models.lta import LTASite, LTARequest, LTAError, LTARequestStatus, AffectedSurls, \
    link_surls_to_lta_request, LTAMacaroon


class LTASiteSerializer(ModelSerializer):
    class Meta:
        model = LTASite
        fields = '__all__'


class LTAErrorSerializer(ModelSerializer):
    class Meta:
        model = LTAError
        fields = '__all__'


class SurlRequestSerializer(ModelSerializer):
    class Meta:
        model = AffectedSurls
        fields = '__all__'


class LTARequestStatusSerializer(ModelSerializer):
    class Meta:
        model = LTARequestStatus
        fields = '__all__'


class LTAMacaroonSerializer(ModelSerializer):
    class Meta:
        model = LTAMacaroon
        fields = '__all__'


class SurlField(StringRelatedField):
    def to_internal_value(self, data):
        url = AffectedSurls(surl=data)
        return url


class LTARequestSerializer(ModelSerializer):
    surls = SurlField(many=True)
    statuses = LTARequestStatusSerializer(many=True, required=False, read_only=True)
    errors = LTAErrorSerializer(many=True, required=False, read_only=True)
    macaroons = LTAMacaroonSerializer(many=True, required=False, read_only=True)

    def create(self, validated_data):
        surls: List[AffectedSurls] = validated_data.pop('surls')
        request = LTARequest(**validated_data)
        request.save()
        request = link_surls_to_lta_request(surls, request)
        return request

    class Meta:
        model = LTARequest
        fields = '__all__'


class DraftLTARequestSerializer(ModelSerializer):
    surls = ListField(child=CharField())

    def validate_surls(self, surls):
        for surl in surls:
            if not surl.startswith('gsiftp://') and not surl.startswith('srm://'):
                raise ValidationError(f'{surl} is an invalid surl: protocol supported are either gsiftp:// or srm://')
        return surls

    class Meta:
        model = DraftLTARequest
        fields = '__all__'
