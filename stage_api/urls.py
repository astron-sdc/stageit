from django.urls import path, include
from graphene_django.views import GraphQLView

from stage_api.views import LTASiteViewset, LTAErrorViewset, StagingRequestViewset, GenerateToken, \
    DraftLTARequestViewset,DRFAuthenticatedGraphQLView, LTAMacaroonViewset, \
    convert_surls_to_webdav, convert_surls_to_gridftp, stageit_metrics_view, stageit_get_lta_site_combined_info_view
from rest_framework.routers import DefaultRouter
from rest_framework.documentation import include_docs_urls

conf = DefaultRouter()
conf.register('lta_site', LTASiteViewset)

main = DefaultRouter()
main.register('requests', StagingRequestViewset, basename='LTARequest'),
main.register('draft', DraftLTARequestViewset),

main.register('errors', LTAErrorViewset)
main.register('macaroons', LTAMacaroonViewset)

# .../stageit/api/staging/...
urlpatterns = [
    path('conf/', include(conf.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('', include(main.urls)),
    path('get-token', GenerateToken.as_view(), name='obtain-api-token'),
    path('docs/', include_docs_urls(title='StageIT API')),
    path('graphql/', DRFAuthenticatedGraphQLView.as_view(graphiql=True)),
    # /stageit/api/staging/requests/<id>/convert2webdav
    path('requests/<int:pk>/convert2webdav/', convert_surls_to_webdav, name='get-webdav-urls'),
    path('requests/<int:pk>/convert2gridftp/', convert_surls_to_gridftp, name='get-gridftp-urls'),
    # override with 'own' so disable -> metrics path("", include("django_prometheus.urls")),
    path('metrics/', stageit_metrics_view, name='get-stageit-metrics'),

    # for some testing metrics/statistics
    path('lta-site-info/', stageit_get_lta_site_combined_info_view, name='get-stageit-ltasite-info'),
]
