from django.contrib import admin

from stage_api.models.email import EmailTemplate, EmailTemplateFile
from stage_api.models.lta import LTASite, LTARequest


@admin.action(description="Repair LTARequest in case of issues")
def update(modeladmin, request, queryset):
    for item in queryset:
        item.update()


class RequestAdmin(admin.ModelAdmin):
    ordering = ["-pk"]
    list_display = ['pk', 'request_type', 'current_status_code', 'user']
    actions = [update]


class EmailTemplateAdmin(admin.ModelAdmin):
    pass


class EmailTemplateFileAdmin(admin.ModelAdmin):
    pass


class LTASiteAdmin(admin.ModelAdmin):
    pass


# Register your models here.
admin.site.register(LTARequest, RequestAdmin)
admin.site.register(EmailTemplate, EmailTemplateAdmin)
admin.site.register(EmailTemplateFile, EmailTemplateFileAdmin)
admin.site.register(LTASite, LTASiteAdmin)
