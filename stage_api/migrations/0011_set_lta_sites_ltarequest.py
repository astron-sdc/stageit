# Generated by Django 4.2.3 on 2023-07-13 09:24

from django.db import migrations


def set_lta_sites(apps, schema_editor):
    """Calls the admin command to set the LTA sites"""

    # https://docs.djangoproject.com/en/3.2/topics/migrations/#data-migrations
    LTARequest = apps.get_model('stage_api', 'LTARequest')
    requests = LTARequest.objects.all()
    for request in requests:
        request.set_lta_sites_from_surls()


class Migration(migrations.Migration):
    dependencies = [("stage_api", "0010_ltarequest_lta_sites"), ]

    operations = [migrations.RunPython(set_lta_sites)]
