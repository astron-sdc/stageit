from django.db import models


class Configuration(models.Model):
    name = models.CharField(max_length=200, primary_key=True)
    value = models.CharField(max_length=2000)

    @staticmethod
    def get_or_default(name, default):
        try:
            return Configuration.objects.get(name=name).value
        except Configuration.DoesNotExist:
            return default
