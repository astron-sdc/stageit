import logging
import re
from datetime import datetime, timedelta, timezone
from typing import List

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import ObjectDoesNotExist, Count, Sum, Subquery, OuterRef, IntegerField
from django.utils.translation import gettext_lazy as _

from stage_api import lta_utils
from stage_api.models.configuration import Configuration
from stage_api.models.user_info import UserInfo, ProjectRole
from stageit.celery import app as celery_app

logger = logging.getLogger(__name__)


def link_surls_to_lta_request(surls: List["AffectedSurls"], request: "LTARequest"):
    for surl in surls:
        surl.request = request
        surl.update_project_info()
        surl.save()

    request.surls.add(*surls, bulk=False)
    request.set_lta_sites_from_surls()
    return request


class LTASite(models.Model):
    name = models.CharField(db_index=True, unique=True, max_length=50)
    dcache_api_url = models.CharField(max_length=1000, null=True)
    dcache_api_token = models.CharField(max_length=1000, null=True)
    surl_pattern = models.CharField(max_length=500, null=True)
    proxy_certificate_path = models.TextField(null=True)
    webdav_base_url = models.CharField(max_length=500, blank=True)
    gridftp_base_url = models.CharField(max_length=500, blank=True)
    download_server_url = models.CharField(max_length=500, blank=True)
    disable_certificate_check = models.BooleanField(default=False)
    valid_time = models.PositiveIntegerField(default=6)   # in days

    @staticmethod
    def get_site_from_surl(surl):
        for site in LTASite.objects.all():
            if re.match(site.surl_pattern, surl):
                return site
        else:
            return None

    def get_download_path(self, surl):
        return re.sub(self.surl_pattern, self.download_server_url, surl)

    def get_gridftp_path(self, surl):
        return re.sub(self.surl_pattern, self.gridftp_base_url, surl)

    def __str__(self):
        return self.name


class LTARequestType(models.TextChoices):
    SIZE = 'size', _('size in byte of surl')
    STAGE = 'stage', _('stage data')
    XATTRS = 'xattrs', _('fetch extended attributes')
    ABORT = 'abort', _('abort stage request')
    RELEASE = 'release', _('release pin'),
    STATS = 'stats', _('file statistics')


class LTARequestStatusCode(models.TextChoices):
    """
    Use the old stager status for UI which means a conversions from stageIT status:
    S(ubmitted)  -> new
    P(ending)    -> scheduled
    R(unning)    -> in progress
    C(ompleted)  -> success
    A(borted)    -> aborted
    E(rror)      -> failed
    I(ncomplete) -> partial success
    """
    SUBMITTED = 'S', _('new')
    PENDING = 'P', _('scheduled')
    RUNNING = 'R', _('in progress')
    ABORTED = 'A', _('aborted')
    ERROR = 'E', _('failed')
    INCOMPLETE = 'I', _('partial success')
    COMPLETED = 'C', _('success')


_STATUS_CODE_TO_TEXT = {status_code: text for status_code, text in LTARequestStatusCode.choices}


class LTARequest(models.Model):
    request_token = models.CharField(max_length=100, default='')
    request_type = models.CharField(max_length=15, choices=LTARequestType.choices)
    when = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(get_user_model(), null=True, on_delete=models.SET_NULL)
    pintime = models.PositiveBigIntegerField(default=0)
    response = models.JSONField(default=dict)
    notify = models.BooleanField(default=False)
    related = models.ForeignKey('LTARequest', default=None, null=True, on_delete=models.SET_NULL)
    lta_sites = models.ManyToManyField("LTASite", blank=True)
    email = models.EmailField(null=True)

    current_status_code = models.CharField(max_length=1, choices=LTARequestStatusCode.choices,
                                           default=LTARequestStatusCode.SUBMITTED)
    retry_count = models.IntegerField(default=0, null=True, blank=True)

    def can_be_restarted(self):
        return self.current_status_code in (
            LTARequestStatusCode.ERROR, LTARequestStatusCode.COMPLETED, LTARequestStatusCode.INCOMPLETE)

    def can_be_aborted(self):
        return self.current_status_code in (
            LTARequestStatusCode.SUBMITTED, LTARequestStatusCode.PENDING, LTARequestStatusCode.RUNNING)

    def set_lta_sites_from_surls(self):
        """Set `lta_sites` based on the related `surls`
        Note: since it uses a ManyToManyField, this operation is idempotent
        """
        for surl in self.surls.all():
            site = LTASite.get_site_from_surl(surl.surl)
            if site:
                self.lta_sites.add(site)

    def update(self):
        """
        Used to fix request that for some reason have not been updated automatically
        in a correct way
        """
        self.current_status_code = self.latest_status.status_code
        self.save(update_fields=['current_status_code'])

        if not self.response:
            if self.errors.count() == 0 and self.current_status_code == LTARequestStatusCode.COMPLETED:
                self.response = {'errors': [], 'progressing': [],
                                 'online': list(self.surls.values_list('surl', flat=True)), 'n_errors': 0,
                                 'n_progressing': 0, 'n_online': self.surls.count(), 'n_finished': self.surls.count()}
                self.save(update_fields=['response'])

    def abort(self, user):
        message = Configuration.get_or_default('admin-abort-message', 'Request aborted by staff')
        message = message if user.is_staff or user.is_superuser else 'Request aborted'
        lta_request_status = LTARequestStatus(request=self, status_code=LTARequestStatusCode.ABORTED, status=message)
        lta_request_status.save()

        abort_request = LTARequest(request_type=LTARequestType.ABORT, related=self, request_token=self.request_token,
                                   user=user)
        abort_request.save()

    def retry_in_place(self):
        self.retry_count += 1
        self.save()
        LTARequestStatus.objects.create(request=self, status_code=LTARequestStatusCode.SUBMITTED)

    @property
    def request_size(self):
        return self.surls.aggregate(total_size=models.Sum('size'))['total_size']

    @property
    def latest_status(self) -> "LTARequestStatus":
        latest_status = self.statuses.all().order_by('-when').first()
        return latest_status

    @property
    def last_status_change(self):
        latest_status = self.latest_status
        latest_status_when = latest_status.when if latest_status is not None else self.when
        return latest_status_when

    @property
    def last_errors(self):
        return self.errors.filter(when__gte=self.last_status_change.isoformat()).order_by('-when')

    @property
    def current_status(self):
        return _STATUS_CODE_TO_TEXT[self.current_status_code]

    @property
    def has_macaroon(self):
        """Does the LTA Request already has at least one macaroon object """
        return self.macaroons.all().count() > 0

    @property
    def lta_macaroon_sites(self):
        """List the names of the LTA Site(s) which has associated macaroons"""
        return [mac.lta_site.name for mac in self.macaroons.all()]

    def macaroon_exists_for_surl(self, surl):
        """Does the macaroon already exist for the given surl a.k.a LTA Site?"""
        lta_site_surl = LTASite.get_site_from_surl(surl)
        if lta_site_surl is not None:
            if lta_site_surl.name in self.lta_macaroon_sites:
                return True
        return False

    def get_macaroon_from_site(self, lta_site):
        """Retrieve the macaroon object for the given LTA Site"""
        matching_macaroon = next((mac for mac in self.macaroons.all() if mac.lta_site == lta_site), None)
        return matching_macaroon

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        try:
            request_before = LTARequest.objects.get(pk=self.pk)
            request_after = self
            from_status = request_before.current_status_code
            to_status = request_after.current_status_code
            super(LTARequest, self).save(force_insert=force_insert, force_update=force_update,
                                         update_fields=update_fields)

            status_has_changed = update_fields is not None and 'current_status_code' in update_fields and from_status != to_status
            if status_has_changed:
                logging.info(f'request status has changed from {from_status} to {to_status}')

                # Importing tasks causes a circular import
                celery_app.send_task('stage_api.tasks.handle_status_change',
                                     args=[self.pk, self.request_type, from_status, to_status])
        except ObjectDoesNotExist:
            super(LTARequest, self).save(force_insert=force_insert, force_update=force_update,
                                         update_fields=update_fields)


class LTARequestStatus(models.Model):
    request = models.ForeignKey(LTARequest, on_delete=models.CASCADE, related_name='statuses')
    when = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1000)
    status_code = models.CharField(max_length=1, choices=LTARequestStatusCode.choices)
    notified = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        before_status = self.request.current_status_code
        after_status = self.status_code
        self.request.current_status_code = self.status_code

        status_has_changed = before_status != after_status
        if status_has_changed:
            self.request.save(update_fields=['current_status_code'])

        super().save(*args, **kwargs)


class AffectedSurls(models.Model):
    surl = models.CharField(max_length=1000)
    request = models.ForeignKey(LTARequest, related_name='surls', on_delete=models.CASCADE)
    size = models.PositiveBigIntegerField(default=0)
    is_user_in_project = models.BooleanField(null=True)

    class Meta:
        indexes = [models.Index(fields=['request', 'surl'], name='request_surl_idx')]

    @property
    def project(self):
        REGEX = r'.*/projects/([^/]*)/.*'
        reg_result = re.search(REGEX, self.surl)
        if reg_result is None:
            return 'undefined'
        groups = reg_result.groups()
        return groups[0]

    def __str__(self):
        return self.surl

    def update_project_info(self):
        try:
            userinfo = UserInfo.objects.get(user=self.request.user)
            ProjectRole.objects.get(user_info=userinfo, project=self.project)
            self.is_user_in_project = True
        except ObjectDoesNotExist:
            self.is_user_in_project = False

    def update_size(self):
        size = lta_utils.size_of_file(self.surl)
        if size:
            self.size = size
            self.save(update_fields=['size'])


class LTAError(models.Model):
    generated_by = models.ForeignKey(LTARequest, on_delete=models.SET_NULL, null=True, related_name='errors')
    lta_site = models.ForeignKey(LTASite, null=True, on_delete=models.CASCADE)
    error = models.CharField(max_length=1000, null=True)
    error_type = models.CharField(max_length=50, null=True)
    when = models.DateTimeField(auto_now_add=True)
    solved = models.BooleanField(default=False)
    acknowledged = models.BooleanField(default=False)
    solved_at = models.DateTimeField(null=True)
    acknowledged_at = models.DateTimeField(null=True)

    def acknowledge(self):
        self.acknowledged = True
        self.acknowledged_at = datetime.now(tz=timezone.utc)
        self.save()

    def solve(self):
        self.solved = True
        self.solved_at = datetime.now(tz=timezone.utc)
        self.save()

    def save(self, *args, **kwargs):
        self.when = datetime.now(tz=timezone.utc)
        if self.generated_by.surls.first():
            self.lta_site = LTASite.get_site_from_surl(self.generated_by.surls.first().surl)

        status = LTARequestStatus(status=f'Error {self.error_type}', when=self.when,
                                  status_code=LTARequestStatusCode.ERROR, request=self.generated_by, notified=False)
        status.save()
        super().save(*args, **kwargs)


class LTAMacaroon(models.Model):
    requested_by = models.ForeignKey(LTARequest, related_name='macaroons', on_delete=models.SET_NULL, null=True, blank=True)
    content = models.CharField(max_length=1000, null=True)
    lta_site = models.ForeignKey(LTASite, null=True, on_delete=models.CASCADE)
    caveat_root_path = models.CharField(max_length=500, blank=True)
    valid_until = models.DateTimeField(null=True)

    def save(self, *args, **kwargs):
        self.valid_until = datetime.now(tz=timezone.utc) + timedelta(days=self.lta_site.valid_time)
        super().save(*args, **kwargs)
