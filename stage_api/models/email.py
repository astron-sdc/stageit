from django.conf import settings
from django.db import models

from stage_api.emails import send_email_from_template, Context, generate_from_template_str, AttachmentFile
from stage_api.models.lta import LTARequestType, LTARequestStatusCode, LTARequest


class EmailTemplate(models.Model):
    class Meta:
        unique_together = ['request_type', 'transition_from_status', 'transition_to_status']
        index_together = ['request_type', 'transition_from_status', 'transition_to_status']

    request_type = models.CharField(max_length=15, choices=LTARequestType.choices)
    template = models.TextField()
    transition_from_status = models.CharField(max_length=1, choices=LTARequestStatusCode.choices)
    transition_to_status = models.CharField(max_length=1, choices=LTARequestStatusCode.choices)
    subject = models.CharField(max_length=200)

    def send(self, lta_request: LTARequest):
        address = lta_request.email if lta_request.email else lta_request.user.email
        attachments = [file_attachment_rule.generate_attachment(lta_request) for file_attachment_rule in
                       self.attachments.all()]

        send_email_from_template(self.template,
                                 context=Context({'lta_request': lta_request, 'host_url': settings.HOST_URL}),
                                 to_address=address, subject=self.subject, attachments=attachments)

    def __str__(self):
        return f'EmailTemplate({self.id})-{self.request_type} from {self.transition_from_status} ' \
               f'to {self.transition_to_status}'


class EmailTemplateFile(models.Model):
    email_template = models.ForeignKey(EmailTemplate, on_delete=models.DO_NOTHING, related_name='attachments')
    template = models.TextField()
    file_name = models.CharField(max_length=100, default='')

    def generate_attachment(self, lta_request: LTARequest):
        content = generate_from_template_str(self.template, Context({'lta_request': lta_request}))
        mimetype = 'plain/text'
        filename = self.file_name
        return AttachmentFile(filename, content, mime=mimetype)
