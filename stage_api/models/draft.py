import uuid

from django.contrib.postgres.fields import ArrayField
from django.db import models

from stage_api.models.lta import LTARequest, AffectedSurls, LTARequestType, link_surls_to_lta_request


class DraftLTARequest(models.Model):
    when = models.DateTimeField(auto_now_add=True)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    surls = ArrayField(models.CharField(max_length=1000))
    request_type = models.CharField(max_length=15, choices=LTARequestType.choices)

    def create_request(self, user):
        surls = [AffectedSurls(surl=surl) for surl in self.surls]
        request = LTARequest(user_id=user, request_type=self.request_type, notify=True)
        request.save()
        request = link_surls_to_lta_request(surls, request)
        self.delete()
        return request
