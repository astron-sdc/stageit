from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.db import models


class UserInfo(models.Model):
    user = models.ForeignKey(get_user_model(), null=True, on_delete=models.SET_NULL)
    location = models.CharField(max_length=100, blank=True, default='', null=True)


class ProjectRole(models.Model):
    user_info = models.ForeignKey(UserInfo, on_delete=models.CASCADE, null=True)
    project = models.CharField(max_length=20, db_index=True)
    roles = ArrayField(models.CharField(max_length=30), default=list)

    def add_role(self, role: "ProjectRole"):
        if role.roles:
            unique_roles = set(self.roles)
            unique_roles.update(role.roles)
            self.roles = list(unique_roles)
            self.save()

    def __str__(self):
        return self.project
