FROM rockylinux:9

RUN yum install -y wget && \
    wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm && \
    rpm -ivh epel-release-latest-9.noarch.rpm && \
    wget http://repository.egi.eu/sw/production/cas/1/current/repo-files/egi-trustanchors.repo && \
    mv egi-trustanchors.repo /etc/yum.repos.d/ && \
    yum install -y voms-clients-cpp myproxy gfal2-python3 gfal2-all ca-policy-egi-core && \
    echo '"lofar" "voms.grid.sara.nl" "30019" "/O=dutchgrid/O=hosts/OU=sara.nl/CN=voms.grid.sara.nl" "lofar"' > /etc/vomses && \
    mkdir /etc/grid-security/vomsdir/lofar && \
    echo "/O=dutchgrid/O=hosts/OU=sara.nl/CN=voms.grid.sara.nl" > /etc/grid-security/vomsdir/lofar/voms.grid.sara.nl.lsc && \
    echo "/C=NL/O=NIKHEF/CN=NIKHEF medium-security certification auth" >> /etc/grid-security/vomsdir/lofar/voms.grid.sara.nl.lsc
RUN yum install -y python3-pip python3-psycopg2 git && pip install setuptools_git_versioning
ADD requirements.txt /opt/stageit/requirements.txt
RUN python3 -m pip install -r /opt/stageit/requirements.txt
ADD stage_api /opt/stageit/stage_api
ADD stageit /opt/stageit/stageit
ADD stageit_ui /opt/stageit/stageit_ui
ADD stageit_statistics /opt/stageit/stageit_statistics
ADD templates /opt/stageit/templates
ADD manage.py /opt/stageit/manage.py
ADD deploy/docker_resources/stageit_entrypoint.sh /opt/entrypoint.sh
ADD deploy/docker_resources/renew_proxy.sh /opt/renew_proxy.sh
ADD fixture /opt/stageit/fixture
RUN chmod +x /opt/renew_proxy.sh /opt/entrypoint.sh
WORKDIR /opt/stageit
ENTRYPOINT ["/opt/entrypoint.sh"]
CMD ["--help"]
