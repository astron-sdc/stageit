#!/bin/bash

set -e

echo container args are "$@"

if [ $# -eq 0 ] || [ $1 == '--help' ]; then
  echo "Stageit"
  echo "Please specify a command:"
  echo "-------------------------"
  echo "workers:  Run celery workers"
  echo "backend: Run backend"
  echo "devel-backend: Run the development backend"
  echo "migrate: Apply the database migrations"
  echo "collect-static: Collect static files"
  echo "loaddata: Load fixtures data"
elif [ $1 == 'workers' ]; then
  /usr/bin/python3 -m celery -A stageit worker -B "${@:2}"
elif [ $1 == 'backend' ]; then
  echo 'Not implemented'
elif [ $1 == 'devel-backend' ]; then
  /usr/bin/python3 manage.py runserver 0.0.0.0:8000 "${@:2}"
elif [ $1 == 'migrate' ]; then
  /usr/bin/python3 manage.py migrate "${@:2}"
elif [ $1 == 'collect-static' ]; then
  /usr/bin/python3 manage.py collectstatic --noinput "${@:2}"
elif [ $1 == 'loaddata' ]; then
  /usr/bin/python3 manage.py loaddata --noinput "${@:2}"
fi

