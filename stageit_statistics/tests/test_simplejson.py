import datetime
import json
from functools import reduce

from django.urls import reverse
from rest_framework.test import APITestCase

from stage_api.models.lta import LTARequest, AffectedSurls
from stageit_statistics.queries import AVAILABLE_QUERIES

_query_request = {'app': 'explore', 'dashboardId': 0, 'timezone': 'browser', 'startTime': 1658759548936,
                  'interval': '60s', 'intervalMs': 60000, 'panelId': 'Q-9b6d778e-5b8f-4927-af7f-53ac56df45fa-0',
                  'targets': [{'target': 'count_staging_requests', 'refId': 'A', 'type': 'timeserie'}],
                  'range': {'from': '2022-07-25T13:32:28.935Z', 'to': '2022-07-25T14:32:28.935Z',
                            'raw': {'from': 'now-1h', 'to': 'now'}}, 'requestId': 'explore_left',
                  'rangeRaw': {'from': 'now-1h', 'to': 'now'},
                  'scopedVars': {'__interval': {'text': '2s', 'value': '2s'},
                                 '__interval_ms': {'text': 2000, 'value': 2000}}, 'maxDataPoints': 1223,
                  'liveStreaming': False, 'adhocFilters': []}


def get_query_request():
    _query_request['range']['from'] = (datetime.datetime.now() - datetime.timedelta(hours=1)).isoformat()
    _query_request['range']['to'] = (datetime.datetime.now()).isoformat()
    return _query_request


_search_request = {'target': 'upper_50'}

test_srm_url = 'srm://srm.surfsara.nl/my/nice/path/to/file.tar.gz'


def add_test_data():
    for i in range(3600):
        request = LTARequest(request_type='stage')
        request.save()

        surl = AffectedSurls(surl=test_srm_url, request=request)
        surl.save()


class TestSimpleJsonRequests(APITestCase):
    def setUpTestData():
        add_test_data()

    def test_search_post(self):
        response = self.client.post(reverse("sj-search"))
        self.assertEqual(200, response.status_code)
        self.assertListEqual(list(AVAILABLE_QUERIES.keys()), response.data)

    def test_query_post(self):
        response = self.client.post(reverse("sj-query"), json.dumps(get_query_request()),
                                    content_type='application/json')
        self.assertEqual(200, response.status_code)
        total_requests = reduce(lambda x, y: x + y[0], response.json()[0]['datapoints'], 0)
        self.assertEqual(3600, total_requests)
