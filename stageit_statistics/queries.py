import datetime

import django.db.backends.utils

_MAX_QUERY_ROWS = 1000


def example_query_function(range,
                           interval,
                           filters,
                           max_data):
    raise NotImplementedError()


from django.db import connection


def datetime_to_unixts(ts):
    S_TO_MS = 1000
    return ts.timestamp() * S_TO_MS


def reformat_sample(sample):
    if isinstance(sample, tuple) or isinstance(sample, list):
        return list(sample[:-1]) + [datetime_to_unixts(sample[-1])]
    else:
        raise NotImplementedError()


class Metric:
    name = None
    available_filters = []

    @classmethod
    def query(cls):
        raise NotImplementedError

    def perform(self, start_time, end_time, interval, limit=None):
        with connection.cursor() as cursor:
            cursor.execute(self.query(), dict(start_time=start_time,
                                              end_time=end_time,
                                              interval=interval))
            if limit is None:
                limit = _MAX_QUERY_ROWS

            for _ in range(limit):
                item = cursor.fetchone()
                if item is not None:
                    item = reformat_sample(item)
                    yield item
                else:
                    break


class CountStagingRequests(Metric):
    name = 'count_staging_requests'

    @classmethod
    def query(cls):
        raw = """
        SELECT COUNT(*),
               time_bucket(to_timestamp(%(start_time)s, 'YYYY-MM-DDTHH:MI:SS'), stage_api_ltarequest.when, %(interval)s) as T_BUCKET
        from stage_api_ltarequest
        where "when" between %(start_time)s and %(end_time)s
        GROUP BY T_BUCKET order by T_BUCKET ASC
        """
        return raw


AVAILABLE_QUERIES = {
    'count_staging_requests': CountStagingRequests

}
