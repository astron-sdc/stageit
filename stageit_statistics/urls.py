from django.urls import path
from stageit_statistics.views.simplejson import  Query, Search, Ping
from stageit_statistics.views.rendered import SubmittedStageRequest

urlpatterns = [
    path('submitted_requests/', SubmittedStageRequest.as_view(), name='submitted-requests'),
    path('simplejson/', Ping.as_view(), name='sj-ping'),
    path('simplejson/query', Query.as_view(), name='sj-query'),
    path('simplejson/search', Search.as_view(), name='sj-search'),
    path('simplejson/annotations', Ping.as_view(), name='sj-annotations'),
]
