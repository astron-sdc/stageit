from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from stage_api.models.lta import LTARequest


class IsStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_staff


class SubmittedStageRequest(APIView):
    permission_classes = [IsStaff, permissions.IsAdminUser]

    def get_queryset(self, request):
        from_datetime = request.query_params.get('from', None)
        to_datetime = request.query_params.get('to', None)
        params = {}
        if from_datetime:
            params['when__gte'] = from_datetime
        if to_datetime:
            params['when__lte'] = to_datetime

        return LTARequest.objects.filter(**params)

    def get(self, request, format=None):
        query_set = self.get_queryset(request)
        start_date, end_date = query_set.order_by('when').first().when, query_set.order_by('when').last().when
        interval = (end_date - start_date) / 100
        print(interval)
        query_set = query_set.raw('''
            select * from stage_api_ltarequest
            ''', )
        for q in query_set:
            print(q)
        return Response()
