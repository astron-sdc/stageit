import logging

import rest_framework.request
from rest_framework import permissions
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

from stageit_statistics.queries import AVAILABLE_QUERIES

headers = {"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Methods": "POST",
           "Access-Control-Allow-Headers": "accept, content-type"}


def _range_to_start_datetime_end_datetime(payload):
    if 'range' in payload:
        if 'from' in payload['range']:
            return payload['range']['from'], payload['range']['to']
        elif 'raw' in payload['range']:
            return NotImplementedError()

    if 'rangeRow' in payload:
        return NotImplementedError()


def _interval(payload):
    if 'intervalMs' in payload:
        return int(int(payload['intervalMs']) / 1000)


class IsStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_staff


class Ping(APIView):
    def get(self, request, format=None):
        return Response(data={'status': 'ok'}, headers=headers)


class Search(APIView):
    def post(self, request, *args, **kwargs):
        return Response(data=list(AVAILABLE_QUERIES.keys()), headers=headers)


class Query(APIView):
    def execute_one(self, target, start_time, end_time, interval, limit=None):
        query_name = target['target']
        query = AVAILABLE_QUERIES.get(query_name, None)
        if query:
            datapoints = list(query().perform(start_time, end_time, interval, limit=limit))
        else:
            datapoints = []

        return {'target': target['refId'], 'datapoints': datapoints}

    def post(self, request: rest_framework.request.Request, *args, **kwargs):
        json = request.data
        try:
            query_to_execute = json['targets']
            start_time, end_time = _range_to_start_datetime_end_datetime(json)
            interval = _interval(json)
            limits = json['maxDataPoints']

            format = json.get('format', 'json')
            results = [self.execute_one(target, start_time, end_time, interval, ) for target in query_to_execute]

            if format == 'json':
                return Response(data=results)
            else:
                raise NotImplementedError()
        except Exception as e:
            logging.exception(e)
            raise ValidationError('Invalid request format')
