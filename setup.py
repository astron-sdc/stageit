import setuptools

setuptools.setup()

# with open("README.md", "r") as fh:
#     long_description = fh.read()
#
# with open('requirements.txt', 'r') as req:
#     inst_requires = req.read().splitlines(keepends=False)
#
# setuptools.setup(
#     name="Stagit",
#     version="0.0.1",
#     author="Mattia Mancini",
#     author_email="mancini@astron.nl",
#     description="An web application to facilitate the staging of LOFAR data via DCache and SRM urls",
#     long_description=long_description,
#     long_description_content_type="text/markdown",
#     url="https://git.astron.nl/eosc/stagit.git",
#     packages=setuptools.find_packages(),
#     classifiers=[
#         "Programming Language :: Python :: 3",
#         "License :: OSI Approved :: Apache 2 License",
#         "Operating System :: OS Independent",
#     ],
#     python_requires='>=3.6',
#     install_requires=inst_requires,
#     test_suite='nose.collector',
#     tests_require=['nose']
# )
