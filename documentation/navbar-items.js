const NavbarLogo = (
  `<a href="/stageit" style="margin-left: -25px; text-decoration: none; ">
      <img alt="StageIT" src="/stageit/static/documentation/img/stageit-logo-2.png" width="30" height="32"
           style="vertical-align: middle"/>
      <b style="vertical-align: middle; margin-left: 7px; color: #000; ">StageIT</b>
  </a>`
)

module.exports = NavbarLogo;
