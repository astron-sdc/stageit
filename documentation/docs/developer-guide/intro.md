---
displayed_sidebar: developerGuideSideBar
sidebar_position: 1
---

# Developer Guide Introduction



## The Data Model 

The diagram of the StageIT datamodel is created from the django models and looks as follows:
![The StageIT Datamodel](/img/stageit_models.png)