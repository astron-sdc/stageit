---
sidebar_position: 2
---

# API

StageIT provides an API, which can be browsed by appending `api/staging` to the landing page (i.e., https://sdc.astron.nl/stageit/api/staging/). The endpoints displayed on this page can also be interfaced with programmatically, but you will need an access token. This token can be acquired by clicking on your username in the top right on any page. You will navigate to a new page, with a button labeled `Request token`:

![The "Request token" button](/img/request_token.png)

Clicking this button will yield a token for usage with the API.

## Python module

There is a Python module available for interfacing with the StageIT API:

* The token (mentioned above) is valid indefinitely. Requesting a token multiple times will yield the same token.

* Make sure the token is available in your `~/.stagingrc` file by specifying it in a single line as:
    * `api_token=<YOUR_TOKEN_HERE>`
    * remove the old username and password from your `.stagingrc` file.
    
* The script is `Python2` compatible, there is a `Dockerfile` available for `Python2` testing in `./tests/docker/python2`
    
* The `requests` library is a required dependency. If you care about Python2 compatability, you can use at most version 2.22.0 of requests. Otherwise, you can install any version (note: you can also `pip install -r 'requirements.txt'`, which will (currently) install version 2.22.0)

Also note that some functions are not supported in the new LTA stager. The states that a request can be in have been simplified. As such, there is no need for these functions anymore. Upon use, they will display an error stating that the function is deprecated. Please look at the `stager_access.py` file (in the repository cited below) for more information. 

* The module can be found in the `lofar_stager_api` [repository](https://git.astron.nl/astron-sdc/lofar_stager_api). 

