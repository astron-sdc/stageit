---
sidebar_position: 2
---

# Account

To use StageIT, you will need a LOFAR LTA account. Please refer to the [Lofar Wiki](https://www.astron.nl/lofarwiki/doku.php?id=public:access) for more information on getting access to LOFAR. Signing in to StageIT with the "Sign in" button on the top right of the landing page will prompt you to login with Keycloak using your LOFAR LTA account credentials. If you run into problems logging in, please contact the [SDC Helpdesk](https://support.astron.nl/jira/servicedesk/customer/portal/7).