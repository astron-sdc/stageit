---
sidebar_position: 1.5
---

# Environments

There are currently two environments available for StageIT

- The test environment, which can be reached at https://sdc-dev.astron.nl/stageit
- The production environment, which can be reached at https://sdc.astron.nl/stageit


## WARNING
As a user, you will always want to use the production environment, unless instructed otherwise. The test environment may be used for testing purposes which could lead to instability on that environment. Be aware that by its nature, this environment is meant for experimenting and ***no support*** is provided on its use.