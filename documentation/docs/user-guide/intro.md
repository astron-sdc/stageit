---
displayed_sidebar: userGuideSideBar
sidebar_position: 1
---

# User Guide Introduction

Welcome to the StageIT User Guide. This guide describes the functionality that this application offers and how can it can be used. In the menu on the left, you will find various topics that you can explore. If you have any questions, remarks, or bug reports, please use the [SDC Helpdesk](https://support.astron.nl/jira/servicedesk/customer/portal/7).

### What is StageIT?

LOFAR data is stored on a tape infrastructure at the LOFAR Long Term Archive (LTA) locations. Currently, the LTA locations are: SARA, Juelich and Poznan. Such facilities, use [DCACHE](https://www.dcache.org) (developed by CERN) to mediate the access from the external users to the data itself.

In particular, the data has two possible states: either is stored on tape or it is staged temporarily to be downloaded or accessed on a disk. Accessing the data is not trivial, and this process has been abstracted away into the StageIT application. Using this application, you can easily create requests to stage data and receive notifications when your stage request has been completed.