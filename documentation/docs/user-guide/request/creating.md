---
sidebar_position: 3
---

# Creating a Stage Request

In most cases, a staging request will be created through the LOFAR LTA interface. If you already have a list of surls from the LTA, a stage request can be created by pressing the button with a plus sign at the top of the page:

![The "Create a new stage request" button](/img/create_stage_request.png)

After pressing the button, you will be presented with a new screen which prompts you to fill in some information:

* Either a single surl, or a list of surls separated by newlines.
* The e-mail address used for notifications. This field can be left blank in which case e-mail notifications will be sent to the e-mail address linked to your account.
* A checkbox to enable or disable receiving notifications at all. Tick the notify checkbox if you want the notification to be sent to you when the data is ready. If not provided, StageIT will not send e-mail notifications. 

Once the stage request is submitted, you will be returned to the home page where you can see your request progress into subsequent stages until it is completed. Please see the [states page](../request/states) for an explanation of the different states a request can be in. 