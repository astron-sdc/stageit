---
sidebar_position: 2
---

# The Overview Page

The stage request overview is the landing page of the application and - provided that you are logged in - shows a list of all your stage requests. Your name will appear in the top navigation bar, and to the top left a button with a plus sign to create a new stage request is visible.

Furthermore, you can see all the stage requests you have submitted and if you click on the id you can see the details of each request. The list of requests looks like this: 

![The stage request overview](/img/stage_request_overview.png)

On top of the list, a number of filter fields are present which allow you to filter the list. For example, selecting the value `in progress` from the `Current status code` dropdown and subsequently pressing the `Filter` button will cause the list to only display stage requests with the status `in progress`.
