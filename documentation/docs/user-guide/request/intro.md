---
sidebar_position: 1
---

# Introduction

As stated before, LOFAR data is stored on a tape infrastructure at the LTA locations. It can however be staged temporarily to be downloaded or accessed on a disk. This section of the guide explains how stage requests can be created and viewed. Additionally, there is a section that explains all the states in which a stage request can be.