---
sidebar_position: 6
---

# Macaroons

StageIT is supported with macaroons. A valid Macaroon will be generated per stage request for every LTA site. 
The macaroon can be used to access data via the [WebDAV protocol](http://www.webdav.org). The generated macaroon will be sent within the notification email.
Attached in the e-mail there will be a webdav.txt and a macaroon.txt. The webdav.txt consist of urls to the webdav 
location of the requested files. In combination with the content of the macaroon.txt you will be able to access the 
data easily. Check the [data access paragraph](../data_access/intro) for more information.

The generated macaroon can also be viewed on the bottom of the Stage Request Details. 
It shows also until when the macaroon is valid. Normally this is 6 days.

![The stage request details](/img/stage_request_details.png)





