---
sidebar_position: 4
---

# Viewing a Stage Request

On the overview page, you can click on a request id (in the column labeled `REQUEST ID`) to view the details of a request:

![The "View stage request" page](/img/view_details.png)

This page displays all properties of the stage request, including the status, whether notifications are turned on, and the e-mail address to use for notifications (only when it is different from the one linked to your account).

On the bottom of the page, all specified surls are displayed in the section labeled `Specified surls`. In addition to all surls specified by the user, surls are also displayed grouped by their status in the section `Details`. Please see the [states page](../request/states) for an explanation of the different states a request can be in. 